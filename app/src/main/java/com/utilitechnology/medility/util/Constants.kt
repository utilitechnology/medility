package com.utilitechnology.medility.util

/**
 * Created by Administrator on 06-12-2017.
 */

object Constants {
    var CURRENT_ITEM_KEY = "currentitem"

    enum class UserTypes {
        TYPE_HOSPITAL, TYPE_PHARMACY, TYPE_LAB, TYPE_DOCTOR, TYPE_BLOOD_BANK
    }

    const val PREF_SESSION = "PrefSession"
    const val PREF_USER_ID = "user_id"
    const val PREF_PASSWORD = "password"

    const val INTENT_FACILITY = "facility"

    const val API_ENDPOINT = "http://www.utilitechnology.com/api/medility/"

    const val HOSPITAL_XML_STRING = "hospital"
    const val PHARMACY_XML_STRING = "pharmacy"
    const val LAB_XML_STRING = "clinic"
    const val BLOODBANK_XML_STRING = "bloodbank"
}
