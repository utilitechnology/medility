package com.utilitechnology.medility.util;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.setting.SettingsAPI;
import com.utilitechnology.medility.util.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 05-11-2016.
 */

public class SearchProvider extends ContentProvider {
    public static final String AUTHORITY = "com.utilitechnology.medility.util.SearchProvider";

    public static final Uri SEARCH_URI = Uri.parse("content://" + AUTHORITY + "/search");

    public static final Uri DETAILS_URI = Uri.parse("content://" + AUTHORITY + "/details");

    private static final int SEARCH = 1;
    private static final int SUGGESTIONS = 2;
    private static final int DETAILS = 3;

    private static final UriMatcher mUriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {

        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // URI for "Go" button
        uriMatcher.addURI(AUTHORITY, "search", SEARCH);

        // URI for suggestions in Search Dialog
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SUGGESTIONS);

        // URI for Details
        uriMatcher.addURI(AUTHORITY, "details", DETAILS);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
//        Log.e("important",selection+" and "+selectionArgs[0]);
        Cursor c = null;
        List<String> list = getFacilities(selectionArgs[0]);
        MatrixCursor mCursor = null;

        switch (mUriMatcher.match(uri)) {
            case SEARCH:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_TEXT_2, SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});
                for (int j = 0; j < list.size(); j++) {
                    String totalString = list.get(j);
                    String facName = totalString.split("~")[1].trim();
                    String facDist = totalString.split("~")[2].trim();
                    mCursor.addRow(new String[]{Integer.toString(j), facName, facDist, totalString});
                }
                c = mCursor;
                break;
            case SUGGESTIONS:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_TEXT_2, SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});

                for (int j = 0; j < list.size(); j++) {
                    String totalString = list.get(j);
                    String facName = totalString.split("~")[1].trim();
                    String facDist = totalString.split("~")[2].trim();
                    mCursor.addRow(new String[]{Integer.toString(j), facName, facDist, totalString});
                }
                c = mCursor;
                break;

            case DETAILS:
                mCursor = new MatrixCursor(new String[]{"_id", SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_TEXT_2, SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA});
                for (int j = 0; j < list.size(); j++) {
                    String totalString = list.get(j);
                    String facName = totalString.split("~")[1].trim();
                    String facDist = totalString.split("~")[2].trim();
                    mCursor.addRow(new String[]{Integer.toString(j), facName, facDist, totalString});
                }
                c = mCursor;
                break;
        }
//        Log.e("important", "query: returning cursor items");
        return c;
    }

    private List<String> getFacilities(String suggestion) {
//        Log.e("important", "getFacilities: searching "+suggestion);
        SettingsAPI sta = new SettingsAPI(getContext());
        String urlStr = getContext().getString(R.string.unidom) + "api/medility/search_hospitals.php?param=";
        if (sta.readCurrentItem() == Constants.UserTypes.TYPE_HOSPITAL)
            urlStr = getContext().getString(R.string.unidom) + "api/medility/search_hospitals.php?param=";
        else if (sta.readCurrentItem() == Constants.UserTypes.TYPE_PHARMACY)
            urlStr = getContext().getString(R.string.unidom) + "api/medility/search_pharma.php?param=";
        else if (sta.readCurrentItem() == Constants.UserTypes.TYPE_LAB)
            urlStr = getContext().getString(R.string.unidom) + "api/medility/search_clinic.php?param=";
        else if (sta.readCurrentItem() == Constants.UserTypes.TYPE_BLOOD_BANK)
            urlStr = getContext().getString(R.string.unidom) + "api/medility/search_blood_bank.php?param=";
        List<String> mFac = new ArrayList<>();
        if (suggestion != null) {
            try {
                URL url = new URL(urlStr + suggestion);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                for (String line; (line = reader.readLine()) != null; ) {
//                    String[] lineParts=line.split(",");
//                    mFac.add(lineParts[1]+"["+lineParts[2]+"]");
                    line = line.replace("&amp;", "&");
                    mFac.add(line);
                }
                stream.close();
            } catch (NullPointerException | IOException e) {
                //Log.e("important", "getFacilities: ");
            }
        }
//        Log.e("important", "getFacilities: returning " + mFac.size() + " elements");
        return mFac;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
