package com.utilitechnology.medility.util

import android.content.Context
import android.content.SharedPreferences

/**
Created by bibaswann on 09/10/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class PrefSession(context: Context) {
    private var sharedPreferences: SharedPreferences? = null

    init {
        sharedPreferences = context.getSharedPreferences(Constants.PREF_SESSION, Context.MODE_PRIVATE)
    }

    fun login(userId: String, password: String, userType: String) {
        sharedPreferences!!.edit().putString(Constants.PREF_USER_ID, userId).apply()
        sharedPreferences!!.edit().putString(Constants.PREF_PASSWORD, password).apply()
    }

    fun logout() {
        sharedPreferences!!.edit().clear().apply()
    }

    fun isLoggedIn(): Boolean {
        return getUserId() != null
    }

    fun getUserId(): String? {
        return sharedPreferences!!.getString(Constants.PREF_USER_ID, null)
    }
}