package com.utilitechnology.medility.util

import java.util.*

/**
 * Created by Administrator on 06-12-2017.
 */

object Util {
    fun isStringNotEmpty(string: String?): Boolean {
        return !(string == null || string.isEmpty() || string.length == 0 || string.trim { it <= ' ' } == "")
    }

    fun convertStringToEnum(strVal: String): Constants.UserTypes {
        return Constants.UserTypes.valueOf(strVal)
    }

    fun roundOff(number: String?): String {
        val numNum = java.lang.Float.valueOf(number!!)
        return String.format(Locale("en", "IN"), "%.2f", numNum)
    }
}
