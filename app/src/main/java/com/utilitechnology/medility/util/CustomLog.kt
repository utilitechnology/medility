package com.utilitechnology.medility.util

import android.util.Log
import com.utilitechnology.medility.BuildConfig

/**
Created by bibaswann on 09/10/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


object CustomLog {
    private val logTag = "mfilog"

    fun v(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.v(logTag, msg)
        }
    }

    fun d(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.d(logTag, msg)
        }
    }

    fun i(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.i(logTag, msg)
        }
    }

    fun w(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.w(logTag, msg)
        }
    }

    fun e(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.e(logTag, msg)
        }
    }

    fun wtf(msg: String) {
        if (BuildConfig.DEBUG) {
            Log.wtf(logTag, msg)
        }
    }

    fun printStackTrace(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e(logTag, Log.getStackTraceString(throwable))
        }
    }
}