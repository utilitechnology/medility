package com.utilitechnology.medility.parser;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by B Bandyopadhyay on 24-08-2017.
 */

public class ParseOnlinePharma {
    private List<String> pSer = new ArrayList<>();
    private List<String> pName = new ArrayList<>();
    private List<String> pPhone = new ArrayList<>();
    private List<String> pUrl = new ArrayList<>();
    private List<String> pDp = new ArrayList<>();

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseOnlinePharma(String url) {
        urlString = url;
    }

    public List<String> getpPhone() {
        return pPhone;
    }

    public List<String> getpName() {
        return pName;
    }

    public List<String> getpSer() {
        return pSer;
    }

    public List<String> getpUrl() {
        return pUrl;
    }

    public List<String> getpDp() {
        return pDp;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("serial")) {
                            pSer.add(text);
                        } else if (name.equals("name")) {
                            pName.add(text);
                        } else if (name.equals("dp")) {
                            pDp.add(text);
                        } else if (name.equals("url")) {
                            pUrl.add(text);
                        } else if (name.equals("phone")) {
                            pPhone.add(text);
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
                    //Log.e("important",Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
