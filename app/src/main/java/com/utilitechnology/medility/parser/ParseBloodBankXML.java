package com.utilitechnology.medility.parser;

import com.utilitechnology.medility.model.BloodBankModel;
import com.utilitechnology.medility.util.Util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by B Bandyopadhyay on 25-08-2017.
 */

public class ParseBloodBankXML {
    private BloodBankModel bloodBankModel = new BloodBankModel();

    private String urlString = null;

    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseBloodBankXML(String url) {
        urlString = url;
    }

    public BloodBankModel getBloodBankModel() {
        return bloodBankModel;
    }

    public void setBloodBankModel(BloodBankModel bloodBankModel) {
        this.bloodBankModel = bloodBankModel;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null) {
//                            Log.e("important", "got "+name);
                            switch (name) {
                                case "serial":
                                    bloodBankModel.setSerial(Long.parseLong(text));
                                    break;
                                case "address":
                                    bloodBankModel.setAddress(text);
                                    break;
                                case "name":
                                    bloodBankModel.setName(text);
                                    break;
                                case "pincode":
                                    bloodBankModel.setPincode(text);
                                    break;
                                case "coordinate":
                                    bloodBankModel.setCoordinate(text);
                                    break;
                                case "dp":
                                    bloodBankModel.setDp(text);
                                    break;
                                case "rating":
                                    bloodBankModel.setRating(Util.INSTANCE.roundOff(text));
                                    break;
                                case "rating_count":
                                    bloodBankModel.setTimesRated(text);
                                    break;
                                case "review_count":
                                    bloodBankModel.setTimesReviewed(text);
                                    break;
                                case "category":
                                    bloodBankModel.setCategory(text);
                                    break;
                                case "bloodComponent":
                                    bloodBankModel.setBloodComponent(text);
                                    break;
                                case "apheresis":
                                    bloodBankModel.setApheresis(text);
                                    break;
                                case "telephone":
                                    bloodBankModel.setPhone(text);
                                    break;
                                case "mobile":
                                    bloodBankModel.setMobile(text);
                                    break;
                                case "license":
                                    bloodBankModel.setLicense(text);
                                    break;
                                case "open_hours":
                                    bloodBankModel.setOpenHours(text);
                                    break;
                                case "helpline":
                                    bloodBankModel.setHelpline(text);
                                    break;
                                case "fax":
                                    bloodBankModel.setFax(text);
                                    break;
                                case "email":
                                    bloodBankModel.setEmail(text);
                                    break;
                                case "website":
                                    bloodBankModel.setWebsite(text);
                                    break;
                                case "no_name":
                                    bloodBankModel.setNoName(text);
                                    break;
                                case "no_phone":
                                    bloodBankModel.setNoPhone(text);
                                    break;
                                case "no_mobile":
                                    bloodBankModel.setNoMobile(text);
                                    break;
                                case "no_email":
                                    bloodBankModel.setNoEmail(text);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
//            Log.e("important", Log.getStackTraceString(e));
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
