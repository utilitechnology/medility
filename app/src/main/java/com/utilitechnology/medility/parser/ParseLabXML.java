package com.utilitechnology.medility.parser;

import android.util.Log;

import com.utilitechnology.medility.model.LabModel;
import com.utilitechnology.medility.util.Util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bibaswann on 12-03-2017.
 */

public class ParseLabXML {
    private LabModel labModel = new LabModel();

    private String urlString = null;

    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseLabXML(String url) {
        urlString = url;
    }

    public LabModel getLabModel() {
        return labModel;
    }

    public void setLabModel(LabModel labModel) {
        this.labModel = labModel;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null) {
                            switch (name) {
                                case "serial":
                                    labModel.setSerial(Long.parseLong(text));
                                    break;
                                case "address":
                                    labModel.setAddress(text);
                                    break;
                                case "name":
//                                Log.e("important", "adding "+text);
                                    labModel.setName(text);
                                    break;
                                case "pincode":
                                    labModel.setPincode(text);
                                    break;
                                case "coordinate":
                                    labModel.setCoordinate(text);
                                    break;
                                case "dp":
                                    labModel.setDp(text);
                                    break;
                                case "rating":
                                    labModel.setRating(Util.INSTANCE.roundOff(text));
                                    break;
                                case "rating_count":
                                    labModel.setTimesRated(text);
                                    break;
                                case "review_count":
                                    labModel.setTimesReviewed(text);
                                    break;
                                case "category":
                                    labModel.setCategory(text);
                                    break;
                                case "telephone":
                                    labModel.setPhone(text);
                                    break;
                                case "mobile":
                                    labModel.setMobile(text);
                                    break;
                                case "open_hours":
                                    labModel.setOpenHours(text);
                                    break;
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    Log.e("important", "running");
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
