package com.utilitechnology.medility.parser;

import com.utilitechnology.medility.model.HospitalModel;
import com.utilitechnology.medility.util.CustomLog;
import com.utilitechnology.medility.util.Util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

/**
 * Created by Bibaswann on 18-12-2016.
 */

public class ParseHospitalXML {
    private HospitalModel hospitalModel = new HospitalModel();


    private String urlString = null;

    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseHospitalXML(String url) {
        urlString = url;
    }

    public HospitalModel getHospitalModel() {
        return hospitalModel;
    }

    public void setHospitalModel(HospitalModel hospitalModel) {
        this.hospitalModel = hospitalModel;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null) {
//                            Log.e("important", "got "+name);
                            switch (name) {
                                case "serial":
                                    hospitalModel.setSerial(Long.parseLong(text));
                                    break;
                                case "address":
                                    hospitalModel.setAddress(text);
                                    break;
                                case "name":
                                    hospitalModel.setName(text);
                                    break;
                                case "pincode":
                                    hospitalModel.setPincode(text);
                                    break;
                                case "coordinate":
                                    hospitalModel.setCoordinate(text);
                                    break;
                                case "dp":
                                    hospitalModel.setDp(text);
                                    break;
                                case "rating":
                                    hospitalModel.setRating(Util.INSTANCE.roundOff(text));
                                    break;
                                case "rating_count":
                                    hospitalModel.setTimesRated(text);
                                    break;
                                case "review_count":
                                    hospitalModel.setTimesReviewed(text);
                                    break;
                                case "category":
                                    hospitalModel.setCategory(text);
                                    break;
                                case "discipline":
                                    hospitalModel.setDiscipline(text);
                                    break;
                                case "establishment":
                                    hospitalModel.setEstablishment(text);
                                    break;
                                case "telephone":
                                    hospitalModel.setPhone(text);
                                    break;
                                case "mobile":
                                    hospitalModel.setMobile(text);
                                    break;
                                case "emergency":
                                    hospitalModel.setEmergency(text);
                                    break;
                                case "ambulance":
                                    hospitalModel.setAmbulance(text);
                                    break;
                                case "bloodbank":
                                    hospitalModel.setBloodbank(text);
                                    break;
                                case "tollfree":
                                    hospitalModel.setTollfree(text);
                                    break;
                                case "helpline":
                                    hospitalModel.setHelpline(text);
                                    break;
                                case "fax":
                                    hospitalModel.setFax(text);
                                    break;
                                case "email":
                                    String email = text;
                                    if (email.equals("NA, NA"))
                                        email = "NA";
                                    else if (email.contains("NA"))
                                        email = email.replace(",", "").replace(" ", "").replace("NA", "");
                                    hospitalModel.setEmail(email);
                                    break;
                                case "website":
                                    hospitalModel.setWebsite(text);
                                    break;
                                case "specialities":
                                    hospitalModel.setSpecialities(text);
                                    break;
                                case "facilities":
                                    hospitalModel.setFacilities(text);
                                    break;
                                case "registration":
                                    hospitalModel.setReg(text);
                                    break;
                                case "estd":
                                    hospitalModel.setEstd(text);
                                    break;
                                case "num_doctors":
                                    hospitalModel.setDoc(text);
                                    break;
                                case "num_beds":
                                    hospitalModel.setBed(text);
                                    break;
                                case "num_pvt_ward":
                                    hospitalModel.setPvtWard(text);
                                    break;
                                case "emergency_services":
                                    hospitalModel.setEmergencyServices(text);
                                    break;
                                case "tariff":
                                    hospitalModel.setTariff(text);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
//            CustomLog.INSTANCE.printStackTrace(e);
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
//                    CustomLog.INSTANCE.printStackTrace(e);
                }
            }
        }).start();
    }
}
