package com.utilitechnology.medility.parser

import com.utilitechnology.medility.model.HospitalModel
import com.utilitechnology.medility.model.MedicalFacilityModel
import com.utilitechnology.medility.util.Constants
import com.utilitechnology.medility.util.Util
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.net.HttpURLConnection
import java.net.URL

/**
Created by bibaswann on 03/12/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class ParseListXML(private var urlString: String?, private var typeString: String?) {
    //This class parses list XML and fetches only name, address, rating, serial and image and puts them in MedicalFacilityModel
    //This will be deleted when retrofit will be implemented
    private var facilityModel: MedicalFacilityModel? = null
    var facilityList = ArrayList<MedicalFacilityModel>()
    private var xmlFactoryObject: XmlPullParserFactory? = null
    @Volatile
    var parsingInComplete = true


    private fun parseXMLAndStoreIt(myParser: XmlPullParser) {
        var event: Int
        var text: String? = null
        try {
            event = myParser.eventType
            while (event != XmlPullParser.END_DOCUMENT) {
                val name = myParser.name
                when (event) {
                    XmlPullParser.START_TAG ->
                        if (name != null) {
                            when (name) {
                                typeString -> facilityModel = MedicalFacilityModel()
                            }
                        }
                    XmlPullParser.TEXT -> text = myParser.text
                    XmlPullParser.END_TAG -> if (name != null) {
                        when (name) {
                            "serial" -> facilityModel!!.serial = java.lang.Long.parseLong(text!!)
                            "address" -> facilityModel!!.address = text
                            "name" -> facilityModel!!.name = text
                            "dp" -> facilityModel!!.img = text
                            "rating" -> facilityModel!!.rating = Util.roundOff(text)
                            typeString -> {
                                facilityModel!!.type = when (typeString) {
                                    Constants.HOSPITAL_XML_STRING -> Constants.UserTypes.TYPE_HOSPITAL
                                    Constants.PHARMACY_XML_STRING -> Constants.UserTypes.TYPE_PHARMACY
                                    Constants.LAB_XML_STRING -> Constants.UserTypes.TYPE_LAB
                                    Constants.BLOODBANK_XML_STRING -> Constants.UserTypes.TYPE_BLOOD_BANK
                                    else -> null
                                }
                                facilityList.add(facilityModel!!)
                            }
                        }
                    }
                }
                event = myParser.next()
            }
            parsingInComplete = false
        } catch (e: Exception) {
            //            Log.e("important", Log.getStackTraceString(e));
        }

    }

    fun fetchXML() {
        Thread(Runnable {
            try {
                val url = URL(urlString!!)
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "GET"
                conn.doInput = true
                conn.connect()
                val stream = conn.inputStream
                xmlFactoryObject = XmlPullParserFactory.newInstance()
                //xmlFactoryObject.setNamespaceAware(true);
                val myparser = xmlFactoryObject!!.newPullParser()
                myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                myparser.setInput(stream, null)
                parseXMLAndStoreIt(myparser)
                stream.close()
            } catch (e: Exception) {
                //                    Log.e("important", Log.getStackTraceString(e));
            }
        }).start()
    }
}