package com.utilitechnology.medility.parser;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 11-12-2016.
 */

public class ParseReviewXML {
    private List<String> serial = new ArrayList<>();
    private List<String> rating = new ArrayList<>();
    private List<String> title = new ArrayList<>();
    private List<String> body = new ArrayList<>();
    private List<String> reviewer = new ArrayList<>();
    private List<String> reviewed = new ArrayList<>();

    private String urlString = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParseReviewXML(String url) {
        urlString = url;
    }

    public List<String> getBody() {
        return body;
    }

    public List<String> getRating() {
        return rating;
    }

    public List<String> getReviewer() {
        return reviewer;
    }

    public List<String> getReviewed() {
        return reviewed;
    }

    public List<String> getSerial() {
        return serial;
    }

    public List<String> getTitle() {
        return title;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null)
                            if (name.equals("serial")) {
                                serial.add(text);
                            } else if (name.equals("rating")) {
                                rating.add(text);
                            } else if (name.equals("title")) {
                                title.add(text);
                            } else if (name.equals("text")) {
                                body.add(text);
                            } else if (name.equals("reviewer")) {
                                reviewer.add(text);
                            } else if (name.equals("reviewed")) {
                                reviewed.add(text);
                            }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
//            Log.e("important", Log.getStackTraceString(e));
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
