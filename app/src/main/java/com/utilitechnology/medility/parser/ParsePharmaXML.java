package com.utilitechnology.medility.parser;

/**
 * Created by Bibaswann on 30-10-2016.
 */


import com.utilitechnology.medility.model.PharmaModel;
import com.utilitechnology.medility.util.Util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

/**
 * Created by Bibaswann on 24-03-2016.
 */
public class ParsePharmaXML {
    private PharmaModel pharmaModel = new PharmaModel();

    private String urlString = null;

    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingInComplete = true;

    public ParsePharmaXML(String url) {
        urlString = url;
    }

    public PharmaModel getPharmaModel() {
        return pharmaModel;
    }

    public void setPharmaModel(PharmaModel pharmaModel) {
        this.pharmaModel = pharmaModel;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text = null;
        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name != null && text != null) {
                            switch (name) {
                                case "serial":
                                    pharmaModel.setSerial(Long.parseLong(text));
                                    break;
                                case "address":
                                    pharmaModel.setAddress(text);
                                    break;
                                case "name":
//                                Log.e("important", "adding "+text);
                                    pharmaModel.setName(text);
                                    break;
                                case "pincode":
                                    pharmaModel.setPincode(text);
                                    break;
                                case "coordinate":
                                    pharmaModel.setCoordinate(text);
                                    break;
                                case "dp":
                                    pharmaModel.setDp(text);
                                    break;
                                case "rating":
                                    pharmaModel.setRating(Util.INSTANCE.roundOff(text));
                                    break;
                                case "rating_count":
                                    pharmaModel.setTimesRated(text);
                                    break;
                                case "review_count":
                                    pharmaModel.setTimesReviewed(text);
                                    break;
                                case "discipline":
                                    pharmaModel.setDiscipline(text);
                                    break;
                                case "telephone":
                                    pharmaModel.setPhone(text);
                                    break;
                                case "mobile":
                                    pharmaModel.setMobile(text);
                                    break;
                                case "open24":
                                    pharmaModel.set24(text);
                                    break;
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            parsingInComplete = false;
        } catch (Exception e) {
        }
    }

    public void fetchXML() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
//                    Log.e("important", "running");
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    //xmlFactoryObject.setNamespaceAware(true);
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();
                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);
                    parseXMLAndStoreIt(myparser);
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
