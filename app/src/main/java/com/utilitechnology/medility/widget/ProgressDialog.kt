package com.utilitechnology.medility.widget

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.utilitechnology.medility.R
import com.victor.loading.rotate.RotateLoading

/**
Created by bibaswann on 06/12/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class ProgressDialog {
    private var itp: RotateLoading? = null
    private var dialog: Dialog? = null
    //Todo use identifier for multiple dialogs
    fun showDialog(context: Context) {
        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.setContentView(R.layout.widget_progress_dialog)

        itp = dialog!!.findViewById<RotateLoading>(R.id.rotateLoading)
        itp!!.start()

        dialog!!.show()
    }

    fun hideDialog() {
        itp!!.stop()
        dialog!!.hide()
    }
}