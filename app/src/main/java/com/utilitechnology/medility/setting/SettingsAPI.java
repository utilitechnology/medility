package com.utilitechnology.medility.setting;

import android.content.Context;
import android.content.SharedPreferences;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.util.Constants;

/**
 * Created by Bibaswann on 07-12-2016.
 */

public class SettingsAPI {
    Context mContext;
    private SharedPreferences sharedSettings;

    public SettingsAPI(Context context) {
        mContext = context;
        sharedSettings = mContext.getSharedPreferences(mContext.getString(R.string.settings_file_name), Context.MODE_PRIVATE);
    }

    public String readSetting(String key) {
        return sharedSettings.getString(key, "na");
    }

    public Constants.UserTypes readCurrentItem() {
        //This is how to convert to enum value from saved string
        return Constants.UserTypes.valueOf(sharedSettings.getString(Constants.INSTANCE.getCURRENT_ITEM_KEY(), null));
    }

    public void addUpdateSettings(String key, String value) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void addCurrentItem(Constants.UserTypes value) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString(Constants.INSTANCE.getCURRENT_ITEM_KEY(), value.toString());
        editor.commit();
    }

    public void deleteSettings(String key) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.remove(key);
        editor.apply();
    }

    public void deleteAllSettings() {
        sharedSettings.edit().clear().apply();
    }
}

