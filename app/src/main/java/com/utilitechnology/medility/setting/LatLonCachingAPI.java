package com.utilitechnology.medility.setting;

import android.content.Context;
import android.content.SharedPreferences;

import com.utilitechnology.medility.R;

/**
 * Created by Bibaswann on 25-12-2016.
 */

public class LatLonCachingAPI {
    Context mContext;
    private SharedPreferences sharedSettings;

    public LatLonCachingAPI(Context context) {
        mContext = context;
        sharedSettings = mContext.getSharedPreferences(mContext.getString(R.string.lat_lon_file_name), Context.MODE_PRIVATE);
    }

    public String readLat() {
        return sharedSettings.getString("lat", "NA");
    }

    public String readLng() {
        return sharedSettings.getString("lon", "NA");
    }

    public String readPlace() {
        return sharedSettings.getString("place", "NA");
    }

    public String readPincode() {
        return sharedSettings.getString("pin", "NA");
    }

    public String readReadablePlace() {
        return sharedSettings.getString("readableplace", "NA");
    }

    public void addLatLon(Double lat, Double lon) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString("lat", String.valueOf(lat));
        editor.putString("lon", String.valueOf(lon));
        editor.apply();
    }

    public void addPlace(String place, String from) {
        SharedPreferences.Editor editor = sharedSettings.edit();
//        place = place.trim().replaceAll("^,+", "").replaceAll(",+$", "");
        place=place.trim().replace(", , ",", ");//some values in database are missing, so extra , may come and will be added with the , in placesearchadapter
//        place=place.replaceAll(" +", " ");
        editor.putString("place", place);
        if (place.contains(",")) {
            String[] placeparts = place.split(",");
            if (placeparts.length >= 3 && from.equals("global"))
                editor.putString("readableplace", placeparts[1].trim() + ", " + placeparts[2].trim());
            else
                editor.putString("readableplace", placeparts[0].trim() + ", " + placeparts[1].trim());
        } else {
            editor.putString("readableplace", place);
        }
        editor.apply();
    }

    public void addPinCode(String pin) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString("pin", String.valueOf(pin));
        editor.apply();
    }

    public void deleteAllLatLon() {
        sharedSettings.edit().clear().apply();
    }
}

