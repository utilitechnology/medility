package com.utilitechnology.medility.setting;

import android.content.Context;
import android.content.SharedPreferences;

import com.utilitechnology.medility.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bibaswann on 11-03-2017.
 */

public class BookmarkAPI {
    Context mContext;
    private SharedPreferences sharedSettings;

    public BookmarkAPI(Context context) {
        mContext = context;
        sharedSettings = mContext.getSharedPreferences(mContext.getString(R.string.bookmark_file_name), Context.MODE_PRIVATE);
    }

    public String readSetting(String key) {
        return sharedSettings.getString(key, "NA");
    }

    public void addUpdateSettings(String key, String value) {
        SharedPreferences.Editor editor = sharedSettings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public Map readAllSettings() {
        Map<String, String> savedEntries = new HashMap<>();
        Map<String, ?> allEntries = sharedSettings.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            savedEntries.put(entry.getKey(), entry.getValue().toString());
        }
        return savedEntries;
    }

    public boolean bookMarkExists(String value) {
        Map<String, String> savedEntries = new HashMap<>();
        Map<String, ?> allEntries = sharedSettings.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (value.equals(entry.getValue().toString()))
                return true;
        }
        return false;
    }

    public String getKey(String value) {
        Map<String, String> savedEntries = new HashMap<>();
        Map<String, ?> allEntries = sharedSettings.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            if (value.equals(entry.getValue().toString()))
                return entry.getKey();
        }
        return "NA";
    }

    public void deteleSetting(String key) {
        sharedSettings.edit().remove(key).apply();
    }

    public void deleteAllSettings() {
        sharedSettings.edit().clear().apply();
    }
}
