package com.utilitechnology.medility.api

import com.google.gson.JsonObject
import com.utilitechnology.medility.model.BaseModel

interface ApiResult {

    fun onError(e: Exception)

    fun onModel(baseModel: BaseModel)

    fun onJson(jsonObject: JsonObject)

    fun onAPIFail()
}
