package com.utilitechnology.medility.api

import com.utilitechnology.medility.model.HospitalModel
import com.utilitechnology.medility.model.MedicalFacilityModel
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface WebServiceInterface {
    //Todo Create single URL for all list and all search
    @GET("list_hospitals.php?")
    fun getFacilityList(@Query("page") page: String, @Query("pincode") pincode: String): Observable<MedicalFacilityModel>
}
