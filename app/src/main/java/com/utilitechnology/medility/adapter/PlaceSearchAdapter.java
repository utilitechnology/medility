package com.utilitechnology.medility.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.utilitechnology.medility.setting.LatLonCachingAPI;
import com.utilitechnology.medility.R;

/**
 * Created by Bibaswann on 26-12-2016.
 */

public class PlaceSearchAdapter extends BaseAdapter {
    String[] completeText;
    Context mContext;
    LatLonCachingAPI llc;

    public PlaceSearchAdapter(Context context, String[] completeText) {
        mContext = context;
        this.completeText = completeText;
        llc = new LatLonCachingAPI(mContext);
    }

    @Override
    public int getCount() {
        return completeText.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        //Todo: use recycleview viewholder pattern
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.place_search_adapter, null);

        final TextView topText = rowView.findViewById(R.id.shortPlace);
        final TextView bottomText = rowView.findViewById(R.id.longPlace);

        //+ and - are to be set here when not scraped from xml
        try {
            final String[] completeTextParts = completeText[position].split("~");
            final String address1 = completeTextParts[0].trim();
            final String address2 = completeTextParts[1].trim();

            topText.setText(address1);
            bottomText.setText(address2);


            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Double lat = Double.parseDouble(completeTextParts[2].split(",")[0]);
                    final Double lon = Double.parseDouble(completeTextParts[2].split(",")[1]);
                    final String pincode = completeTextParts[1].split("-")[completeTextParts[1].split("-").length - 1].trim();
                    llc.deleteAllLatLon();
                    llc.addPlace(address1 + ", " + address2,"local");
                    llc.addLatLon(lat, lon);
                    llc.addPinCode(pincode);
                    Intent retIntent = new Intent();
                    retIntent.putExtra("place", address1 + ", " + address2);
                    ((Activity) mContext).setResult(Activity.RESULT_OK, retIntent);
                    ((Activity) mContext).finish();
                }
            });
        } catch (IndexOutOfBoundsException ignored) {
        }

        return rowView;
    }
}
