package com.utilitechnology.medility.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilitechnology.medility.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bibaswann on 10-12-2016.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {
    private List<String> iName;
    private List<String> iRat;
    private List<String> iTitle;
    private List<String> iText;

    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, title, text;
        List<ImageView> stars = new ArrayList<>();

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);
            ImageView singleStar;
            singleStar = view.findViewById(R.id.star0);
            stars.add(singleStar);
            singleStar = view.findViewById(R.id.star1);
            stars.add(singleStar);
            singleStar = view.findViewById(R.id.star2);
            stars.add(singleStar);
            singleStar = view.findViewById(R.id.star3);
            stars.add(singleStar);
            singleStar = view.findViewById(R.id.star4);
            stars.add(singleStar);
        }
    }

    public ReviewAdapter(Context context, List<String> names, List<String> ratings, List<String> titles, List<String> texts) {
        iName = names;
        iRat = ratings;
        iTitle = titles;
        iText = texts;
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public ReviewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_adapter, parent, false);
        return new ReviewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        try {
            holder.name.setText(iName.get(position));
            if (iTitle.get(position).equals("NA")) {
                holder.title.setVisibility(View.GONE);
                holder.text.setVisibility(View.GONE);
            } else {
                holder.title.setText(iTitle.get(position));
                holder.text.setText(iText.get(position));
            }
            Float integeRating = Float.valueOf(iRat.get(position));
            for (int i = 0; i < integeRating; i++) {
                holder.stars.get(i).setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.outline_star_24));
            }

        } catch (IndexOutOfBoundsException iobx) {
            //Log.e("important", Log.getStackTraceString(iobx));
        }
    }

    @Override
    public int getItemCount() {
        return iTitle.size();
    }
}
