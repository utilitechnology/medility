package com.utilitechnology.medility.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import com.squareup.picasso.Picasso
import com.utilitechnology.medility.activity.DetailActivity
import com.utilitechnology.medility.R
import com.utilitechnology.medility.model.MedicalFacilityModel
import com.utilitechnology.medility.util.Constants

/**
 * Created by Bibaswann on 28-10-2016.
 */

class RecyclerAdapter(internal var mContext: Context, internal var mFacilities: List<MedicalFacilityModel>) : RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView
        var address: TextView
        var rating: TextView
        internal var thumg: ImageView
        internal var total: LinearLayout

        init {
            total = view.findViewById(R.id.totalItem)
            name = view.findViewById(R.id.name)
            address = view.findViewById(R.id.address)
            rating = view.findViewById(R.id.rating)
            thumg = view.findViewById(R.id.thumb)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_adapter, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
            holder.name.text = mFacilities[position].name
            holder.address.text = mFacilities[position].address
            holder.rating.text = mFacilities[position].rating

            val dpath = mContext.getString(R.string.unidom) + mFacilities[position].img!!
            val establishmentType = mFacilities[position].type
            if (establishmentType === Constants.UserTypes.TYPE_HOSPITAL)
                Picasso.with(mContext).load(dpath).placeholder(R.drawable.bed).error(R.drawable.bed).into(holder.thumg)
            else if (establishmentType === Constants.UserTypes.TYPE_PHARMACY)
                Picasso.with(mContext).load(dpath).placeholder(R.drawable.pill).error(R.drawable.pill).into(holder.thumg)
            else if (establishmentType === Constants.UserTypes.TYPE_LAB)
                Picasso.with(mContext).load(dpath).placeholder(R.drawable.lab).error(R.drawable.lab).into(holder.thumg)
            else if (establishmentType === Constants.UserTypes.TYPE_BLOOD_BANK)
                Picasso.with(mContext).load(dpath).placeholder(R.drawable.bloodbank).error(R.drawable.bloodbank).into(holder.thumg)

            holder.total.setOnClickListener {
                val details = Intent(mContext, DetailActivity::class.java)
                details.putExtra(Constants.INTENT_FACILITY, mFacilities[position])
                mContext.startActivity(details)
            }
        } catch (iobx: IndexOutOfBoundsException) {
            //Log.e("important", Log.getStackTraceString(iobx));
        }

    }

    override fun getItemCount(): Int {
        return mFacilities.size
    }
}
