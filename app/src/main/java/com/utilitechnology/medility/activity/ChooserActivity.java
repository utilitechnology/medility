package com.utilitechnology.medility.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.setting.LatLonCachingAPI;
import com.utilitechnology.medility.setting.SettingsAPI;
import com.utilitechnology.medility.util.Constants;
import com.utilitechnology.medility.util.CustomToast;
import com.utilitechnology.medility.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ChooserActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_ACCESS_LOCATION = 1000;
    private static final int REQUEST_CHECK_SETTINGS = 2000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 3000;
    private static final int PLACE_SEARCH_REQUEST_CODE = 4000;
    private static final int LOGIN_REQUEST_CODE = 5000;

    double latitude, longitude;
    boolean hasSavedLocation = false;

    CustomToast customToast;

    TextView address;
    Button changeAddress;
    Button emergencyCall;

    TextView welcome;
    Button loginout;

    SettingsAPI set;
    LatLonCachingAPI llc;
    //GPSTracker gps;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;

    ProgressBar addrProg;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chooser);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();
//        drawer.openDrawer(GravityCompat.START);
//        drawer.closeDrawer(GravityCompat.START);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        set = new SettingsAPI(this);
        llc = new LatLonCachingAPI(this);
        customToast = new CustomToast(this);

        ImageButton hos = findViewById(R.id.hosBtn);
        ImageButton phr = findViewById(R.id.phrmBtn);
        ImageButton lab = findViewById(R.id.laBtn);
        ImageButton bb = findViewById(R.id.bbBtn);
        address = findViewById(R.id.addr);
        addrProg = findViewById(R.id.addressProgress);
        changeAddress = findViewById(R.id.chngLoc);
        emergencyCall = findViewById(R.id.emergency);
        emergencyCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:112"));
                startActivity(intent);
            }
        });

        changeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChooserActivity.this, PlaceSearchActivity.class);
                startActivityForResult(i, PLACE_SEARCH_REQUEST_CODE);
            }
        });

        /**The following lines are using GPSTracker class*/
//        gps = new GPSTracker(this);
//        if(gps.canGetLocation()){
//
//            double latitude = gps.getLatitude();
//            double longitude = gps.getLongitude();
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
//        }else{
//            gps.showSettingsAlert();
//        }

        if (!llc.readReadablePlace().equals("NA")) {
            addrProg.setVisibility(View.GONE);
            address.setVisibility(View.VISIBLE);
            address.setText(llc.readReadablePlace());
            hasSavedLocation = true;
        }
        initLocationTracking();

        //Remember: the type must be same as the database name
        hos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openList(Constants.UserTypes.TYPE_HOSPITAL);
            }
        });
        phr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openList(Constants.UserTypes.TYPE_PHARMACY);
            }
        });
        lab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openList(Constants.UserTypes.TYPE_LAB);
            }
        });
        bb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openList(Constants.UserTypes.TYPE_BLOOD_BANK);
            }
        });
    }

    // TODO: 08-12-2017 In android 6 and up, the first time location is not coming, and on reloading the activity, it comes, the reason remains a mystry

    private void initLocationTracking() {
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        welcome = findViewById(R.id.welcomeTxt);
        loginout = findViewById(R.id.login);

        if (isLoggedIn()) {
            welcome.setText(set.readSetting("name"));
            loginout.setText(getString(R.string.logout));
        }

        loginout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoggedIn()) {
                    set.deleteSettings("name");
                    set.deleteSettings("uid");
                    welcome.setText(R.string.welcome_guest);
                    loginout.setText(getString(R.string.login));
                } else {
                    Intent register = new Intent(ChooserActivity.this, RegisterActivity.class);
                    register.putExtra("serial", "NA");
                    register.putExtra("type", "NA");
                    register.putExtra("review", false);
                    startActivityForResult(register, LOGIN_REQUEST_CODE);
                }
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation mView item clicks here.
        int id = item.getItemId();
//Use commitAllowingStateLoss to avoid exception
        if (id == R.id.nav_bookmark) {
            Intent i = new Intent(ChooserActivity.this, BookmarkActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_reviews) {
            if (isLoggedIn()) {
                Intent i = new Intent(ChooserActivity.this, MyReviewActivity.class);
                startActivity(i);
            } else
                customToast.showError("You must login first");
        } else if (id == R.id.nav_order) {
            Intent i = new Intent(ChooserActivity.this, OnlinePharmaActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_feedback) {
            Intent i = new Intent(ChooserActivity.this, FeedbackActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_rate) {
            Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                //Snackbar.make(getCurrentFocus(), "This app is not downloaded from google play", Snackbar.LENGTH_LONG).show();
            }
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openList(Constants.UserTypes type) {
        set.addCurrentItem(type);
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /**
     * This method is for showing alert dialog just like Ola
     */
    public void requestLocation() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        initLocationTracking();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ChooserActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            showLocationError();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix this
                        showLocationError();
                        break;
                }
            }
        });
    }

    /**
     * Permission result
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initLocationTracking();
                } else {
                    //// TODO: 25-12-2016 Program the program to run without location
                    showLocationError();
                }
                break;
        }

    }

    /**
     * Has the user turned on GPS?
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                if (resultCode == RESULT_OK) {
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            displayLocation();
//                        }
//                    }, 10000);
                    while (mLastLocation == null)
                        initLocationTracking();
                } else if (resultCode == RESULT_CANCELED) {
//                        requestLocation();//keep asking if imp or do whatever
                    showLocationError();
                }
                break;
            case PLACE_SEARCH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String message = data.getStringExtra("place");
                    addrProg.setVisibility(View.GONE);
                    address.setVisibility(View.VISIBLE);
                    address.setText(llc.readReadablePlace());
                } else if (resultCode == RESULT_CANCELED) {
                    if (llc.readPlace().equals("NA"))
                        showLocationError();
                }
                break;
            case LOGIN_REQUEST_CODE:
                String name = data.getStringExtra("name");
                welcome.setText(name);
                loginout.setText(getString(R.string.logout));
                break;
        }
    }

    private void showLocationError() {
        addrProg.setVisibility(View.GONE);
        address.setVisibility(View.VISIBLE);
        address.setText(R.string.gps_not_found);
        customToast.showError(getString(R.string.no_location_warning));
    }

    /**
     * All the following methods are absolutely necessary for detecting location and address using Google play services API. No need to register anything in any console. And yes, don't forget the manifests and the implemented interfaces
     */

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_LOCATION);
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            new DownloadAddress().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (!hasSavedLocation)
                    requestLocation();
            } else {
                addrProg.setVisibility(View.GONE);
                address.setVisibility(View.VISIBLE);
                if (!hasSavedLocation)
                    address.setText(R.string.gps_not_found);
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                addrProg.setVisibility(View.GONE);
                address.setVisibility(View.VISIBLE);
                address.setText(R.string.gps_not_found);
                customToast.showError("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        checkPlayServices();
//    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        addrProg.setVisibility(View.GONE);
        address.setVisibility(View.VISIBLE);
        address.setText(R.string.gps_not_found);
        customToast.showError("Connection failed");
    }

    @Override
    public void onConnected(Bundle arg0) {
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    private class DownloadAddress extends AsyncTask<Void, Void, String> {

        private void showLoc(String strAddr) {
            addrProg.setVisibility(View.GONE);
            address.setVisibility(View.VISIBLE);
            address.setText(llc.readReadablePlace());
        }

        private void saveLocation(double latitude, double longitude, String strAddr, String pinCode) {
            llc.deleteAllLatLon();
            llc.addLatLon(latitude, longitude);
            llc.addPlace(strAddr, "global");
            llc.addPinCode(pinCode);
        }

        protected void onPostExecute(final String strAddr) {
            if (hasSavedLocation) {
                if (!strAddr.equals(ChooserActivity.this.getString(R.string.gps_not_found)) && Util.INSTANCE.isStringNotEmpty(strAddr)) {
                    if (!llc.readPlace().equals(strAddr)) {
                        if (!ChooserActivity.this.isFinishing()) {
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ChooserActivity.this);
                            dlgAlert.setMessage("We have detected your current location as " + strAddr + ". Do you want to set this as your current location");
                            dlgAlert.setTitle("New location detected");
                            dlgAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    showLoc(strAddr);
                                }
                            });
                            dlgAlert.setNegativeButton("No", null);
                            dlgAlert.setCancelable(true);
                            dlgAlert.create().show();
                        }
                    }
                }
            } else {
                showLoc(strAddr);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            List<Address> addresses = null;

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                Address currentAddress = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                // Fetch the address lines using getAddressLine,
                // join them, and send them to the thread.
//                for (int i = 0; i < currentAddress.getMaxAddressLineIndex(); i++) {
//                    addressFragments.add(currentAddress.getAddressLine(i).trim());
//                }
//                return TextUtils.join(", ", addressFragments);
                // TODO: 06-12-2017 implement a logic to check if some parts are empty (sometimes null is also coming as first part)

                StringBuilder readableAddress = new StringBuilder();

                if (currentAddress.getThoroughfare() != null && !currentAddress.getThoroughfare().isEmpty())
                    readableAddress.append(currentAddress.getThoroughfare()).append(", ");
                if (currentAddress.getSubLocality() != null && !currentAddress.getSubLocality().isEmpty())
                    readableAddress.append(currentAddress.getSubLocality()).append(", ");
                if (currentAddress.getSubAdminArea() != null && !currentAddress.getSubAdminArea().isEmpty())
                    readableAddress.append(currentAddress.getSubAdminArea()).append(", ");
                if (currentAddress.getLocality() != null && !currentAddress.getLocality().isEmpty())
                    readableAddress.append(currentAddress.getLocality());

                saveLocation(latitude, longitude, readableAddress.toString().trim(), currentAddress.getPostalCode());
                return readableAddress.toString().trim();

            } catch (IOException | IndexOutOfBoundsException e) {
                return getString(R.string.gps_not_found);
            }
        }
    }

    private boolean isLoggedIn() {
        return !set.readSetting("uid").equals("na");
    }
}
