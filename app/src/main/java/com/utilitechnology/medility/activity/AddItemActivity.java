package com.utilitechnology.medility.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.utilitechnology.medility.R;

public class AddItemActivity extends AppCompatActivity {

    static final int REQUEST_CODE_PHOTO_PICKER = 1;
    private static final int REQUEST_CODE_ACCESS_CARD = 2;
    FloatingActionButton submit;
    Button uploadImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        submit = findViewById(R.id.fab_submit);
        uploadImg = findViewById(R.id.uploadImg);

        uploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (ActivityCompat.checkSelfPermission(AddItemActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddItemActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
//                    ActivityCompat.requestPermissions(AddItemActivity.this,
//                            new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_ACCESS_CARD);
//                else
//                    PhotoPicker.startPhotoPickerForResult(AddItemActivity.this, REQUEST_CODE_PHOTO_PICKER);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_ACCESS_CARD)
        {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
//                PhotoPicker.startPhotoPickerForResult(this, REQUEST_CODE_PHOTO_PICKER);
//            else
//                Snackbar.make(findViewById(android.R.id.content),"Please grant access to photos",Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PHOTO_PICKER) {
            if (resultCode == Activity.RESULT_OK) {
//                final Photo[] photos = PhotoPicker.getResultPhotos(data);
//                final List<Uri> resultUris = new ArrayList<>();
////                Log.e("important", "User selected " + photos.length + " photos");
//
//                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
//                dlgAlert.setMessage("Do you want to upload "+photos.length+" images?");
//                dlgAlert.setTitle("Are you sure?");
//                dlgAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        for (Photo p:photos) {
//                            Log.e("important", p.getUri().toString());
//                            String filePath = SiliCompressor.with(AddItemActivity.this).compress(p.getUri().toString());
//                            resultUris.add(Uri.parse(filePath));
//                        }
//                    }
//                });
//                dlgAlert.setNegativeButton("No", null);
//                dlgAlert.setCancelable(true);
//                dlgAlert.create().show();

            } else if (resultCode == Activity.RESULT_CANCELED) {
            } else {}
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
