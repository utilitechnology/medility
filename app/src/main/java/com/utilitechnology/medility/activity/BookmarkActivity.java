package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.fetcher.FetchBookmark;
import com.utilitechnology.medility.model.MedicalFacilityModel;

import java.util.ArrayList;

public class BookmarkActivity extends AppCompatActivity {

    RecyclerView bookList;
    ProgressBar bookProgress;
    RecyclerAdapter bookAdapter;
    ImageView sadSmiley;

    private ArrayList<MedicalFacilityModel> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bookList = findViewById(R.id.bookMarkRecycler);
        bookProgress = findViewById(R.id.bmProg);
        sadSmiley = findViewById(R.id.empty_smiley);
    }

    @Override
    protected void onResume() {
        bookAdapter = new RecyclerAdapter(this, items);
        bookList.setAdapter(bookAdapter);
        bookList.setKeepScreenOn(true);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        bookList.setLayoutManager(mLayoutManager);
        bookList.setItemAnimator(new DefaultItemAnimator());

        new FetchBookmark(this, bookProgress, bookAdapter, sadSmiley, items).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        super.onResume();
    }
}
