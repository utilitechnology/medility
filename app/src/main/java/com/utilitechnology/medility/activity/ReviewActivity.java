package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.parser.ParseReviewXML;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ReviewActivity extends AppCompatActivity {

    String serial, type, uid;
    MaterialEditText title, txt;
    Button submit;
    Float ratingGiven = 0f;
    MaterialRatingBar rate;
    String head = "NA", body = "NA";
    String submitSuccess;
    boolean readSuccess = false;
    String sbRvwUri;
    String rdRvwUri;
    String editSerial="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rate = findViewById(R.id.ratingBar);
        title = findViewById(R.id.rvwTitle);
        txt = findViewById(R.id.rvwTxt);
        submit = findViewById(R.id.rvwDone);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            serial = extra.getString("serial");
            type = extra.getString("type");
            uid = extra.getString("uid");
        }
        //Check if this user has written a review

        rdRvwUri = getString(R.string.unidom) + "api/medility/read_review.php?serial=" + serial + "&type=" + type + "&uid=" + uid;
        new readReview().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        //Log.e("important", "serial received "+uid);

        rate.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                ratingGiven = rating;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratingGiven == 0)
                    Snackbar.make(view, "Please give a star rating", Snackbar.LENGTH_LONG).show();
                else if (title.getText().toString().equals("") && !txt.getText().toString().equals(""))
                    Snackbar.make(view, "Please provide a title", Snackbar.LENGTH_LONG).show();
                else if (!title.getText().toString().equals("") && txt.getText().toString().equals(""))
                    Snackbar.make(view, "Please provide a description", Snackbar.LENGTH_LONG).show();
                else {
                    if (!title.getText().toString().equals("") && !txt.getText().toString().equals("")) {
                        head = title.getText().toString().trim();
                        body = txt.getText().toString().trim();
                    }
                    try {
                        sbRvwUri = getString(R.string.unidom) + "api/medility/write_review.php?rating=" + ratingGiven + "&title=" + URLEncoder.encode(head, "UTF-8") + "&text=" + URLEncoder.encode(body, "UTF-8") + "&target=" + type + "&tserial=" + serial + "&rserial=" + uid + "&eserial=" + editSerial;
                    } catch (UnsupportedEncodingException ignored) {}
                    new submitReview().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        //Log.e("important", "onCreate: serial "+ serial+" type "+type+" uid "+uid);
    }

    private class submitReview extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            submit.setText(R.string.uniLod);
            submit.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(sbRvwUri);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder result = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    result.append(line);
                }
                submitSuccess = result.toString();
                stream.close();
            } catch (Exception e) {
                //Log.e("important", Log.getStackTraceString(e));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (submitSuccess.equals("Success"))
                ReviewActivity.this.finish();
            else {
                submit.setText(R.string.try_again);
                submit.setEnabled(true);
            }
            super.onPostExecute(aVoid);
        }
    }

    private class readReview extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ParseReviewXML prx = new ParseReviewXML(rdRvwUri);
            prx.fetchXML();
            while (prx.parsingInComplete) ;
            if (prx.getSerial().size() > 0) {
                ratingGiven = Float.parseFloat(prx.getRating().get(0));
                head = prx.getTitle().get(0);
                body = prx.getBody().get(0);
                editSerial=prx.getSerial().get(0);
                readSuccess = true;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (readSuccess) {
                rate.setRating(ratingGiven);
                title.setText(head);
                txt.setText(body);
            }
            super.onPostExecute(aVoid);
        }
    }
}
