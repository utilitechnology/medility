package com.utilitechnology.medility.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.model.MedicalFacilityModel;
import com.utilitechnology.medility.setting.LatLonCachingAPI;
import com.utilitechnology.medility.setting.SettingsAPI;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.fetcher.FetchItems;
import com.utilitechnology.medility.util.Constants;
import com.utilitechnology.medility.util.CustomToast;
import com.utilitechnology.medility.widget.ProgressDialog;
import com.victor.loading.rotate.RotateLoading;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int PLACE_SEARCH_REQUEST_CODE = 4000;
    TextView actionBarTxt;
    ImageButton actionbarArrow;
    AsyncTask fetchItems;
    int popPage = 0;
    Constants.UserTypes lisType;
    RecyclerView itemList;
    ProgressDialog itp;

    LatLonCachingAPI llc;
    SettingsAPI set;

    int pastVisiblesItems;
    int visibleItemCount;
    int totalItemCount;

    CustomToast customToast;

    private ArrayList<MedicalFacilityModel> mFacilities = new ArrayList<>();

    private RecyclerAdapter rAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actb = getSupportActionBar();
        actb.setDisplayShowCustomEnabled(true);
        View cView = getLayoutInflater().inflate(R.layout.main_bar_layout, null);

        llc = new LatLonCachingAPI(this);
        set=new SettingsAPI(this);

        customToast = new CustomToast(this);

        actionBarTxt = cView.findViewById(R.id.addr);
        actionbarArrow = cView.findViewById(R.id.locationDown);
        actionbarArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, PlaceSearchActivity.class);
                startActivityForResult(i, PLACE_SEARCH_REQUEST_CODE);
            }
        });
        if (!llc.readReadablePlace().equals("NA"))
            actionBarTxt.setText(llc.readReadablePlace());

        actb.setCustomView(cView);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        itemList = findViewById(R.id.liveRecycler);
        itp = new ProgressDialog();
        lisType = set.readCurrentItem();

        rAdapter = new RecyclerAdapter(this, mFacilities);
        itemList.setAdapter(rAdapter);
        itemList.setKeepScreenOn(true);

        fetchItems = new FetchItems(this, itp, rAdapter, lisType, String.valueOf(popPage), mFacilities).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        //final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        itemList.setLayoutManager(mLayoutManager);
        itemList.setItemAnimator(new DefaultItemAnimator());
        //todo check itemdecorations
        //itemList.addItemDecoration(new DividerItemDecoration(this, ));

        itemList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        popPage += 1;
                        try {
                            new FetchItems(MainActivity.this, itp, rAdapter, lisType, String.valueOf(popPage), mFacilities).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                        catch (Exception ex)
                        {
                            reportAProblem(Log.getStackTraceString(ex));
                        }
                        //Log.e("important", "fetching page " + popPage);
                    }
                }
            }
        });

//        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            String query = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
//            Log.e("important", query);
            Intent detailsIntent = new Intent(getApplicationContext(), DetailActivity.class);
            String[] queryParts = query.split("~");
            // TODO: 27/11/18 create facility object here
            detailsIntent.putExtra("serial", queryParts[0]);
            detailsIntent.putExtra("type", Integer.parseInt(queryParts[3]));
            detailsIntent.putExtra("name", queryParts[1]);
            detailsIntent.putExtra("dpath", queryParts[4]);
            startActivity(detailsIntent);
        }
    }

    private void doSearch(String query) {
        Intent i=new Intent(this,SearchResultActivity.class);
        i.putExtra("type",lisType);
        i.putExtra("param",query);
        startActivity(i);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
//        else if (id == R.id.action_add) {
//            Intent intent = new Intent(this, AddItemActivity.class);
//            startActivity(intent);
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PLACE_SEARCH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    String message = data.getStringExtra("place");
                    actionBarTxt.setText(message);
                } else if (resultCode == RESULT_CANCELED) {
                    customToast.showError(getString(R.string.no_location_warning));
                }
        }
    }

    @Override
    protected void onDestroy() {
        fetchItems.cancel(true);
        super.onDestroy();
    }

    private void reportAProblem(final String msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String complainUrl=getString(R.string.unidom) + "feedback/medility_problem_report.php?msg=" + URLEncoder.encode(msg,"UTF-8");
                    URL url = new URL(complainUrl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream stream = conn.getInputStream();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuilder result = new StringBuilder();
                    for (String line; (line = reader.readLine()) != null; ) {
                        result.append(line);
                    }
                    stream.close();
                } catch (Exception e) {
//                    Log.e("important", Log.getStackTraceString(e));
                }
            }
        }).start();
    }
}
