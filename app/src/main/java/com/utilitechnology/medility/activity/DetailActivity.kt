package com.utilitechnology.medility.activity

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.utilitechnology.medility.R
import com.utilitechnology.medility.model.*
import com.utilitechnology.medility.parser.*
import com.utilitechnology.medility.setting.BookmarkAPI
import com.utilitechnology.medility.util.Constants
import com.utilitechnology.medility.util.CustomToast
import com.utilitechnology.medility.util.Util
import com.utilitechnology.medility.widget.ProgressDialog
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class DetailActivity : AppCompatActivity() {
    lateinit var customToast: CustomToast
    lateinit var bkm: BookmarkAPI
    private var mFacility: MedicalFacilityModel? = null
    internal var type: Constants.UserTypes? = null
    var progressDialog: ProgressDialog = ProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        customToast = CustomToast(this)

        val extra = intent.extras
        if (extra != null) {
            mFacility = extra.getSerializable(Constants.INTENT_FACILITY) as MedicalFacilityModel
        }

        setDp(getString(R.string.unidom) + mFacility!!.img!!)
        type = mFacility!!.type

        when {
            type === Constants.UserTypes.TYPE_HOSPITAL -> FetchHospitalDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            type === Constants.UserTypes.TYPE_PHARMACY -> FetchPharmaDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            type === Constants.UserTypes.TYPE_LAB -> FetchLabDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            type === Constants.UserTypes.TYPE_BLOOD_BANK -> FetchBloodBankDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

            //Log.e("important", "showing "+serial+" of "+ type);

//        detailsLayout = findViewById(R.id.detailsLayout)
//        contactLayout = findViewById(R.id.contactLayoutLayout)
//        nav = findViewById(R.id.navLayout)
//        extras = findViewById(R.id.extraLayout)
//        review = findViewById(R.id.reviewLayout)
//        officer = findViewById(R.id.nodaLayout)

            //// TODO: 11-03-2017 do something with mib
        }

        supportActionBar!!.title = mFacility!!.name
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        bkm = BookmarkAPI(this)

        //Log.e("important", "showing "+serial+" of "+ type);
        mainContent.visibility = View.GONE
        progressDialog.showDialog(this)

//        detailsLayout = findViewById(R.id.detailsLayout)
//        contactLayout = findViewById(R.id.contactLayoutLayout)
//        nav = findViewById(R.id.navLayout)
//        extras = findViewById(R.id.extraLayout)
//        review = findViewById(R.id.reviewLayout)
//        officer = findViewById(R.id.nodaLayout)

        val bookVal = type.toString() + "," + mFacility!!.serial
        if (bkm.bookMarkExists(bookVal))
            bookMark.text = getString(R.string.bookmark_ed)
        else
            bookMark.text = getString(R.string.bookmark)

        bookMark.setOnClickListener {
            val unikey = System.currentTimeMillis().toString()
            val oldKey = bkm.getKey(bookVal)
            if (bookMark.text.toString() == getString(R.string.bookmark)) {
                bkm.addUpdateSettings(unikey, bookVal)
                bookMark.text = getString(R.string.bookmark_ed)
                customToast.showSuccess("Bookmark added, tap again to delete")
            } else {
                bkm.deteleSetting(oldKey)
                bookMark.text = getString(R.string.bookmark)
                customToast.showSuccess("Bookmark deleted")
            }
        }

        //// TODO: 11-03-2017 do something with mib

        reportInfo.setOnClickListener {
            val addr = "utilitechnology@gmail.com, info@utilitechnology.com, bibaswann.mail@gmail.com"
            val sub = "Wrong info of " + mFacility!!.name + ", serial: " + mFacility!!.serial + " (app: medility)"
            composeEmail(addr, sub)
        }

        writeReview.setOnClickListener {
            val register = Intent(this@DetailActivity, RegisterActivity::class.java)
            register.putExtra("serial", mFacility!!.serial)
            register.putExtra("type", type)
            register.putExtra("review", true)
            startActivity(register)
        }

        detailsViewAllReviews.setOnClickListener {
            val allReview = Intent(this@DetailActivity, AllReviewActivity::class.java)
            allReview.putExtra("serial", mFacility!!.serial)
            allReview.putExtra("type", type)
            startActivity(allReview)
        }
    }

    // TODO: 27/11/18 change to retrofit
    private inner class FetchPharmaDetails internal constructor() : AsyncTask<Void?, Void?, Void?>() {
        private val fetchUrl: String
        private var pharmaModel: PharmaModel? = null

        init {
            fetchUrl = this@DetailActivity.getString(R.string.unidom) + "api/medility/details_pharma.php?serial=" + mFacility!!.serial
        }

        override fun doInBackground(vararg voids: Void?): Void? {
            val pfx = ParsePharmaXML(fetchUrl)
            pfx.fetchXML()
            while (pfx.parsingInComplete);
            pharmaModel = pfx.pharmaModel
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            extraLayout.visibility = View.GONE
            categoryLayout.visibility = View.GONE
            openHourLayout.visibility = View.GONE
            establishmentLayout.visibility = View.GONE
            emergencyLayout.visibility = View.GONE
            ambulanceLayout.visibility = View.GONE
            bloodbankLayout.visibility = View.GONE
            helplineLayout.visibility = View.GONE
            tollfreeLayout.visibility = View.GONE
            faxLayout.visibility = View.GONE
            emailLayout.visibility = View.GONE
            websiteLayout.visibility = View.GONE
            bcLayout.visibility = View.GONE
            apLayout.visibility = View.GONE
            licLayout.visibility = View.GONE
            nodaLayout.visibility = View.GONE

            if (pharmaModel!!.phone == "NA" && pharmaModel!!.mobile == "NA")
                contactLayout.visibility = View.GONE
            if (pharmaModel!!.phone == "NA")
                phoneLayout.visibility = View.GONE
            if (pharmaModel!!.mobile == "NA")
                mobileLayout.visibility = View.GONE

            if (pharmaModel!!.discipline == "NA" && pharmaModel!!.is24 == "NA")
                detailsLayout.visibility = View.GONE
            if (pharmaModel!!.discipline == "NA")
                disciplineLayout.visibility = View.GONE
            if (pharmaModel!!.is24 == "NA")
                is24Layout.visibility = View.GONE

            discipline.text = pharmaModel!!.discipline
            if (pharmaModel!!.is24 == "1")
                is24.text = "Yes"
            else
                is24.text = "No"
            tvPhone.text = pharmaModel!!.phone
            tvMobile.text = pharmaModel!!.mobile
            tvAddress.text = pharmaModel!!.address + ", " + pharmaModel!!.pincode
            tvStarred.text = pharmaModel!!.rating!!
            tvRated.text = pharmaModel!!.timesRated
            tvReviewed.text = pharmaModel!!.timesReviewed

            if (pharmaModel!!.timesReviewed == "0")
                detailsViewAllReviews.visibility = View.INVISIBLE

            gNav.setOnClickListener {
                val intent: Intent
                if (pharmaModel!!.coordinate == "NA")
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + pharmaModel!!.address!!))
                else
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + pharmaModel!!.coordinate!!))
                startActivity(intent)
            }

            callPhone.setOnClickListener { callPhone(pharmaModel!!.phone!!) }
            callMobile.setOnClickListener { callPhone(pharmaModel!!.mobile!!) }
            if (pharmaModel!!.phone != "NA")
                call.setOnClickListener { callPhone(pharmaModel!!.phone!!) }
            else
                call.visibility = View.GONE

            FetchImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            FetchMIA().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

            progressDialog.hideDialog()
            mainContent.visibility = View.VISIBLE
            super.onPostExecute(aVoid)
        }
    }

    private inner class FetchHospitalDetails internal constructor() : AsyncTask<Void?, Void?, Void?>() {
        private val fetchUrl: String
        private var hospitalModel: HospitalModel? = null

        init {
            fetchUrl = this@DetailActivity.getString(R.string.unidom) + "api/medility/details_hospital.php?serial=" + mFacility!!.serial
        }

        override fun doInBackground(vararg voids: Void?): Void? {
            val phx = ParseHospitalXML(fetchUrl)
            phx.fetchXML()
            while (phx.parsingInComplete);
            hospitalModel = phx.hospitalModel
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            //todo:handle NA, show/hide textboxes,show/hide whole layout if nothing present
            is24Layout.visibility = View.GONE
            openHourLayout.visibility = View.GONE
            bcLayout.visibility = View.GONE
            apLayout.visibility = View.GONE
            licLayout.visibility = View.GONE
            nodaLayout.visibility = View.GONE

            if (hospitalModel!!.discipline == "NA" && hospitalModel!!.category == "NA" && hospitalModel!!.establishment == "NA")
                detailsLayout.visibility = View.GONE
            if (hospitalModel!!.discipline == "NA")
                disciplineLayout.visibility = View.GONE
            if (hospitalModel!!.category == "NA")
                categoryLayout.visibility = View.GONE
            if (hospitalModel!!.establishment == "NA")
                establishmentLayout.visibility = View.GONE

            if (hospitalModel!!.phone == "NA"
                    && hospitalModel!!.mobile == "NA"
                    && hospitalModel!!.emergency == "NA"
                    && hospitalModel!!.ambulance == "NA"
                    && hospitalModel!!.bloodbank == "NA"
                    && hospitalModel!!.helpline == "NA"
                    && hospitalModel!!.tollfree == "NA"
                    && hospitalModel!!.fax == "NA"
                    && hospitalModel!!.email == "NA"
                    && hospitalModel!!.website == "NA")
                contactLayout.visibility = View.GONE
            if (hospitalModel!!.phone == "NA")
                phoneLayout.visibility = View.GONE
            if (hospitalModel!!.mobile == "NA")
                mobileLayout.visibility = View.GONE
            if (hospitalModel!!.emergency == "NA")
                emergencyLayout.visibility = View.GONE
            if (hospitalModel!!.ambulance == "NA")
                ambulanceLayout.visibility = View.GONE
            if (hospitalModel!!.bloodbank == "NA")
                bloodbankLayout.visibility = View.GONE
            if (hospitalModel!!.helpline == "NA")
                helplineLayout.visibility = View.GONE
            if (hospitalModel!!.tollfree == "NA")
                tollfreeLayout.visibility = View.GONE
            if (hospitalModel!!.fax == "NA")
                faxLayout.visibility = View.GONE
            if (hospitalModel!!.email == "NA")
                emailLayout.visibility = View.GONE
            if (hospitalModel!!.website == "NA")
                websiteLayout.visibility = View.GONE

            if (hospitalModel!!.estd == "NA"
                    && hospitalModel!!.specialities == "NA"
                    && hospitalModel!!.facilities == "NA"
                    && hospitalModel!!.doc == "NA"
                    && hospitalModel!!.bed == "NA"
                    && hospitalModel!!.pvtWard == "NA"
                    && hospitalModel!!.emergencyServices == "NA"
                    && hospitalModel!!.tariff == "NA")
                extraLayout.visibility = View.GONE
            if (hospitalModel!!.estd == "NA")
                estdLayout.visibility = View.GONE
            if (hospitalModel!!.specialities == "NA")
                specialitiesLayout.visibility = View.GONE
            if (hospitalModel!!.facilities == "NA")
                facilitiesLayout.visibility = View.GONE
            if (hospitalModel!!.doc == "NA")
                numDocLayout.visibility = View.GONE
            if (hospitalModel!!.bed == "NA")
                numBedLayout.visibility = View.GONE
            if (hospitalModel!!.pvtWard == "NA")
                numPvtLayout.visibility = View.GONE
            if (hospitalModel!!.emergencyServices == "NA")
                emergencyServiceLayout.visibility = View.GONE
            if (hospitalModel!!.tariff == "NA")
                tariffLayout.visibility = View.GONE


            discipline.text = hospitalModel!!.discipline
            tvPhone.text = hospitalModel!!.phone
            tvMobile.text = hospitalModel!!.mobile
            tvAddress.text = hospitalModel!!.address + ", " + hospitalModel!!.pincode
            tvStarred.text = hospitalModel!!.rating!!
            tvRated.text = hospitalModel!!.timesRated
            tvReviewed.text = hospitalModel!!.timesReviewed
            tvCategory.text = hospitalModel!!.category
            establishment.text = hospitalModel!!.establishment
            tvEmergency.text = hospitalModel!!.emergency
            tvAmbulance.text = hospitalModel!!.ambulance
            tvBloodbank.text = hospitalModel!!.bloodbank
            tvHelpline.text = hospitalModel!!.helpline
            tvTollfree.text = hospitalModel!!.tollfree
            tvFax.text = hospitalModel!!.fax
            tvEmail.text = hospitalModel!!.email
            tvWebsite.text = hospitalModel!!.website
            specialities.text = hospitalModel!!.specialities
            facilities.text = hospitalModel!!.facilities
            estd.text = hospitalModel!!.estd
            numDoc.text = hospitalModel!!.doc
            numBed.text = hospitalModel!!.bed
            numPvt.text = hospitalModel!!.pvtWard
            emergencyService.text = hospitalModel!!.emergencyServices
            tariff.text = hospitalModel!!.tariff

            if (hospitalModel!!.timesReviewed == "0")
                detailsViewAllReviews.visibility = View.INVISIBLE

            gNav.setOnClickListener {
                val intent: Intent
                if (hospitalModel!!.coordinate == "NA")
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + hospitalModel!!.address!!))
                else
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + hospitalModel!!.coordinate!!))
                startActivity(intent)
            }

            callPhone.setOnClickListener { callPhone(hospitalModel!!.phone!!) }
            callMobile.setOnClickListener { callPhone(hospitalModel!!.mobile!!) }
            if (hospitalModel!!.phone != "NA")
                call.setOnClickListener { callPhone(hospitalModel!!.phone!!) }
            else
                call.visibility = View.GONE

            callEmergency.setOnClickListener { callPhone(hospitalModel!!.emergency!!) }

            callAmbulance.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + hospitalModel!!.ambulance!!))
                startActivity(intent)
            }

            callBloodbank.setOnClickListener { callPhone(hospitalModel!!.ambulance!!) }

            callHelpline.setOnClickListener { callPhone(hospitalModel!!.helpline!!) }

            callTollfree.setOnClickListener { callPhone(hospitalModel!!.tollfree!!) }

            callEmail.setOnClickListener {
                var emails = hospitalModel!!.email!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val list = ArrayList(Arrays.asList(*emails))
                list.removeAll(listOf("NA"))
                emails = list.toTypedArray()
                composeEmail(android.text.TextUtils.join(",", emails), "")
            }

            callWeb.setOnClickListener { openWebsite(hospitalModel!!.website) }

            FetchImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            FetchMIA().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

            progressDialog.hideDialog()
            mainContent.visibility = View.VISIBLE
            super.onPostExecute(aVoid)
        }
    }

    private inner class FetchLabDetails internal constructor() : AsyncTask<Void?, Void?, Void?>() {
        private val fetchUrl: String
        private var labModel: LabModel? = null

        init {
            fetchUrl = this@DetailActivity.getString(R.string.unidom) + "api/medility/details_clinic.php?serial=" + mFacility!!.serial
        }

        override fun doInBackground(vararg voids: Void?): Void? {
            val plx = ParseLabXML(fetchUrl)
            plx.fetchXML()
            while (plx.parsingInComplete);
            labModel = plx.labModel
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            extraLayout.visibility = View.GONE
            disciplineLayout.visibility = View.GONE
            establishmentLayout.visibility = View.GONE
            emergencyLayout.visibility = View.GONE
            ambulanceLayout.visibility = View.GONE
            bloodbankLayout.visibility = View.GONE
            helplineLayout.visibility = View.GONE
            tollfreeLayout.visibility = View.GONE
            faxLayout.visibility = View.GONE
            emailLayout.visibility = View.GONE
            websiteLayout.visibility = View.GONE
            is24Layout.visibility = View.GONE

            bcLayout.visibility = View.GONE
            apLayout.visibility = View.GONE
            licLayout.visibility = View.GONE
            nodaLayout.visibility = View.GONE

            if (labModel!!.phone == "NA" && labModel!!.mobile == "NA")
                contactLayout.visibility = View.GONE
            if (labModel!!.phone == "NA")
                phoneLayout.visibility = View.GONE
            if (labModel!!.mobile == "NA")
                mobileLayout.visibility = View.GONE

            if (labModel!!.category == "NA" && labModel!!.openHours == "NA")
                detailsLayout.visibility = View.GONE
            if (labModel!!.category == "NA")
                disciplineLayout.visibility = View.GONE
            if (labModel!!.openHours == "NA")
                openHourLayout.visibility = View.GONE

            tvCategory.text = labModel!!.category
            tvOpenHours.text = labModel!!.openHours
            tvPhone.text = labModel!!.phone
            tvMobile.text = labModel!!.mobile
            tvAddress.text = "$labModel!!.address, $labModel!!.pincode"
            tvStarred.text = labModel!!.rating
            tvRated.text = labModel!!.timesRated
            tvReviewed.text = labModel!!.timesReviewed

            if (labModel!!.timesReviewed == "0")
                detailsViewAllReviews.visibility = View.INVISIBLE

            gNav.setOnClickListener {
                val intent: Intent
                if (labModel!!.coordinate == "NA")
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + labModel!!.address!!))
                else
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + labModel!!.coordinate!!))
                startActivity(intent)
            }

            callPhone.setOnClickListener { callPhone(labModel!!.phone!!) }
            callMobile.setOnClickListener { callPhone(labModel!!.mobile!!) }
            if (labModel!!.phone != "NA")
                call.setOnClickListener { callPhone(labModel!!.phone!!) }
            else
                call.visibility = View.GONE

            FetchImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            FetchMIA().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

            progressDialog.hideDialog()
            mainContent.visibility = View.VISIBLE
            super.onPostExecute(aVoid)
        }
    }

    private inner class FetchBloodBankDetails internal constructor() : AsyncTask<Void?, Void?, Void?>() {
        private val fetchUrl: String
        var bloodBankModel: BloodBankModel? = null

        init {
            fetchUrl = this@DetailActivity.getString(R.string.unidom) + "api/medility/details_blood_bank.php?serial=" + mFacility!!.serial
        }

        override fun doInBackground(vararg voids: Void?): Void? {
            val pbx = ParseBloodBankXML(fetchUrl)
            pbx.fetchXML()
            while (pbx.parsingInComplete);
            bloodBankModel = pbx.bloodBankModel
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            emergencyLayout.visibility = View.GONE
            ambulanceLayout.visibility = View.GONE
            bloodbankLayout.visibility = View.GONE
            tollfreeLayout.visibility = View.GONE
            is24Layout.visibility = View.GONE
            disciplineLayout.visibility = View.GONE
            establishmentLayout.visibility = View.GONE
            estdLayout.visibility = View.GONE
            specialitiesLayout.visibility = View.GONE
            facilitiesLayout.visibility = View.GONE
            numBedLayout.visibility = View.GONE
            numDocLayout.visibility = View.GONE
            numPvtLayout.visibility = View.GONE
            tariffLayout.visibility = View.GONE
            emergencyServiceLayout.visibility = View.GONE


            if (bloodBankModel!!.category == "NA" && bloodBankModel!!.openHours == "NA")
                detailsLayout.visibility = View.GONE
            if (bloodBankModel!!.category == "NA")
                categoryLayout.visibility = View.GONE
            if (bloodBankModel!!.openHours == "NA")
                openHourLayout.visibility = View.GONE

            if (bloodBankModel!!.bloodComponent == "NA" && bloodBankModel!!.apheresis == "NA" && bloodBankModel!!.license == "NA")
                extraLayout.visibility = View.GONE
            if (bloodBankModel!!.bloodComponent == "NA")
                bcLayout.visibility = View.GONE
            if (bloodBankModel!!.apheresis == "NA")
                apLayout.visibility = View.GONE
            if (bloodBankModel!!.license == "NA")
                licLayout.visibility = View.GONE

            if (bloodBankModel!!.phone == "NA"
                    && bloodBankModel!!.mobile == "NA"
                    && bloodBankModel!!.helpline == "NA"
                    && bloodBankModel!!.fax == "NA"
                    && bloodBankModel!!.email == "NA"
                    && bloodBankModel!!.website == "NA")
                contactLayout.visibility = View.GONE
            if (bloodBankModel!!.phone == "NA")
                phoneLayout.visibility = View.GONE
            if (bloodBankModel!!.mobile == "NA")
                mobileLayout.visibility = View.GONE
            if (bloodBankModel!!.helpline == "NA")
                helplineLayout.visibility = View.GONE
            if (bloodBankModel!!.fax == "NA")
                faxLayout.visibility = View.GONE
            if (bloodBankModel!!.email == "NA")
                emailLayout.visibility = View.GONE
            if (bloodBankModel!!.website == "NA")
                websiteLayout.visibility = View.GONE

            if (bloodBankModel!!.noName == "NA")
                nodaLayout.visibility = View.GONE
            if (bloodBankModel!!.noPhone == "NA")
                officerPhoneLayout.visibility = View.GONE
            if (bloodBankModel!!.noMobile == "NA")
                officerMobLayout.visibility = View.GONE
            if (bloodBankModel!!.noEmail == "NA")
                officerEmaiLayout.visibility = View.GONE

            tvPhone.text = bloodBankModel!!.phone
            tvMobile.text = bloodBankModel!!.mobile
            tvAddress.text = "$bloodBankModel!!.address, $bloodBankModel!!.pincode"
            tvStarred.text = bloodBankModel!!.rating
            tvRated.text = bloodBankModel!!.timesRated
            tvReviewed.text = bloodBankModel!!.timesReviewed
            tvCategory.text = bloodBankModel!!.category
            tvHelpline.text = bloodBankModel!!.helpline
            tvFax.text = bloodBankModel!!.fax
            tvEmail.text = bloodBankModel!!.email
            tvWebsite.text = bloodBankModel!!.website
            tvOpenHours.text = bloodBankModel!!.openHours

            tvNoName.text = bloodBankModel!!.noName
            tvNoPhone.text = bloodBankModel!!.noPhone
            tvNoEmail.text = bloodBankModel!!.noEmail
            tvNoMobile.text = bloodBankModel!!.noMobile

            tvBloodComponenet.text = bloodBankModel!!.bloodComponent
            tvApheresis.text = bloodBankModel!!.apheresis
            tvLicense.text = bloodBankModel!!.license

            if (bloodBankModel!!.timesReviewed == "0")
                detailsViewAllReviews.visibility = View.INVISIBLE

            gNav.setOnClickListener {
                val intent: Intent
                if (bloodBankModel!!.coordinate == "NA")
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + bloodBankModel!!.address!!))
                else
                    intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + bloodBankModel!!.coordinate!!))
                startActivity(intent)
            }

            callPhone.setOnClickListener { callPhone(bloodBankModel!!.phone!!) }
            callMobile.setOnClickListener { callPhone(bloodBankModel!!.mobile!!) }
            callHelpline.setOnClickListener { callPhone(bloodBankModel!!.helpline!!) }


            if (bloodBankModel!!.phone != "NA")
                call.setOnClickListener { callPhone(bloodBankModel!!.phone!!) }
            else
                call.visibility = View.GONE

            callEmail.setOnClickListener {
                var emails = bloodBankModel!!.email!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val list = ArrayList(Arrays.asList(*emails))
                list.removeAll(listOf("NA"))
                emails = list.toTypedArray()
                composeEmail(android.text.TextUtils.join(",", emails), "")
            }

            callWeb.setOnClickListener { openWebsite(bloodBankModel!!.website) }

            callNoPhone.setOnClickListener { callPhone(bloodBankModel!!.noPhone!!) }
            callNoMobile.setOnClickListener { callPhone(bloodBankModel!!.noMobile!!) }
            callNoEmail.setOnClickListener {
                var emails = bloodBankModel!!.noEmail!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val list = ArrayList(Arrays.asList(*emails))
                list.removeAll(listOf("NA"))
                emails = list.toTypedArray()
                composeEmail(android.text.TextUtils.join(",", emails), "")
            }

            FetchImages().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            FetchMIA().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

            progressDialog.hideDialog()
            mainContent.visibility = View.VISIBLE
            super.onPostExecute(aVoid)
        }
    }

    private inner class FetchMIA internal constructor() : AsyncTask<Void?, Void?, Void?>() {
        private val fetchUrl: String
        private var numOfActions: Int = 0
        private var call: String? = null
        private var web: String? = null
        private var email: String? = null

        init {
            fetchUrl = this@DetailActivity.getString(R.string.unidom) + "api/medility/mib.php?type=" + type + "&serial=" + mFacility!!.serial
        }

        override fun doInBackground(vararg voids: Void?): Void? {
            val pm = ParseMIA(fetchUrl)
            pm.fetchXML()
            while (pm.parsingInComplete);
            numOfActions = pm.numOfActions
            if (numOfActions > 0) {
                call = pm.call
                web = pm.web
                email = pm.email
            }
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            if (numOfActions > 0) {
                when (type) {
                    Constants.UserTypes.TYPE_HOSPITAL -> mib.text = "Book Appointment"
                    Constants.UserTypes.TYPE_PHARMACY -> mib.text = "Order Medicine"
                    Constants.UserTypes.TYPE_LAB -> mib.text = "Book Test"
                    else -> {
                    }
                }
                mib.setOnClickListener {
                    val modes = ArrayList<String>()
                    if (call != "na")
                        modes.add("Call: " + call!!)
                    if (email != "na")
                        modes.add("Email: " + email!!)
                    if (web != "na")
                        modes.add("Open: " + web!!)

                    val builder = AlertDialog.Builder(this@DetailActivity)
                    builder.setTitle("Choose mode")
                    builder.setItems(modes.toTypedArray()) { dialog, which ->
                        when (which) {
                            0 -> callPhone(call!!)
                            1 -> composeEmail(email, "")
                            2 -> openWebsite(web)
                            else -> {
                            }
                        }
                    }
                    builder.show()
                }
                mib.visibility = View.VISIBLE
            }
            super.onPostExecute(aVoid)
        }
    }

    private inner class FetchImages : AsyncTask<Void?, Void?, Void?>() {
        internal var fullImgPath = this@DetailActivity.getString(R.string.unidom) + "api/medility/get_images.php?type=" + type + "&serial=" + mFacility!!.serial
        internal var images: MutableList<String> = ArrayList()

        override fun doInBackground(vararg voids: Void?): Void? {
            try {
                val url = URL(fullImgPath)
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "GET"
                conn.doInput = true
                conn.connect()
                val stream = conn.inputStream

                val reader = BufferedReader(InputStreamReader(stream))
                var line: String?
                do {
                    line = reader.readLine()
                    if (line == null)
                        break
                    images.add(line)
                } while (true)
                stream.close()
            } catch (e: NullPointerException) {
                //Log.e("important", "getFacilities: ");
            } catch (e: IOException) {
            }

            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            if (images.size > 0) {
                if (images[0] != "NA") {
                    val actualImgPath = this@DetailActivity.getString(R.string.unidom) + images[0]
                    setDp(actualImgPath)
                }
            }
            super.onPostExecute(aVoid)
        }
    }

    private fun setDp(dp: String) {
        Thread(Runnable {
            try {
                val drawableDp = drawableFromUrl(dp)
                runOnUiThread {
                    if (drawableDp != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            toolbar_layout.background = drawableDp
                        } else {
                            toolbar_layout.setBackgroundDrawable(drawableDp)
                        }
                    } else {
                        when (type) {
                            Constants.UserTypes.TYPE_BLOOD_BANK -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                                toolbar_layout.background = ContextCompat.getDrawable(this@DetailActivity, R.drawable.bloodbank)
                            else
                                toolbar_layout.setBackgroundDrawable(ContextCompat.getDrawable(this@DetailActivity, R.drawable.bloodbank))
                            Constants.UserTypes.TYPE_LAB -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                                toolbar_layout.background = ContextCompat.getDrawable(this@DetailActivity, R.drawable.lab)
                            else
                                toolbar_layout.setBackgroundDrawable(ContextCompat.getDrawable(this@DetailActivity, R.drawable.lab))
                            Constants.UserTypes.TYPE_PHARMACY -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                                toolbar_layout.background = ContextCompat.getDrawable(this@DetailActivity, R.drawable.pill)
                            else
                                toolbar_layout.setBackgroundDrawable(ContextCompat.getDrawable(this@DetailActivity, R.drawable.pill))
                            Constants.UserTypes.TYPE_HOSPITAL -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                                toolbar_layout.background = ContextCompat.getDrawable(this@DetailActivity, R.drawable.bed)
                            else
                                toolbar_layout.setBackgroundDrawable(ContextCompat.getDrawable(this@DetailActivity, R.drawable.bed))
                        }
                    }
                }
            } catch (ignored: IOException) {
            }
        }).start()
    }

    @Throws(IOException::class)
    internal fun drawableFromUrl(url: String): Drawable? {
        val x: Bitmap
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.connect()
        if (connection.responseCode == HttpURLConnection.HTTP_NOT_FOUND)
            return null
        val input = connection.inputStream
        x = BitmapFactory.decodeStream(input)
        return BitmapDrawable(this.resources, x)
    }

    private fun callPhone(phone: String) {
        val numToCall = arrayOfNulls<String>(1)
        if (phone.contains(",")) {
            val phones = phone.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val builder = AlertDialog.Builder(this@DetailActivity)
            builder.setTitle("Choose number")
            builder.setItems(phones) { dialog, which -> numToCall[0] = phones[which].trim { it <= ' ' } }
            builder.show()
        } else {
            numToCall[0] = phone.trim { it <= ' ' }
        }
        if (!Util.isStringNotEmpty(numToCall[0]) || numToCall[0]!!.matches(".*[a-z].*".toRegex()) || numToCall[0]!!.length > 14) {
            customToast.showError("Number not in right format")
        } else {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + numToCall[0]))
            startActivity(intent)
        }
    }

    fun composeEmail(address: String?, subject: String) {
        val sendTo: Array<String>
        if (address!!.contains(","))
            sendTo = address.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        else
            sendTo = arrayOf(address)
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:") // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, sendTo)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    fun openWebsite(website: String?) {
        var website = website
        try {
            val i = Intent(Intent.ACTION_VIEW)
            if (!website!!.contains("http"))
                website = "http://$website"
            i.data = Uri.parse(website)
            startActivity(i)
        } catch (anfe: ActivityNotFoundException) {
            customToast.showError("Please install a supported web browser")
        }

    }
}
