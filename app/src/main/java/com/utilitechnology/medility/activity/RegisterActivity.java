package com.utilitechnology.medility.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.setting.SettingsAPI;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class RegisterActivity extends AppCompatActivity {

    Button done;
    MaterialEditText name, email, mobile, pass, confpass;
    String rgstUri;
    String lgUri;
    String serial, type, uid;
    boolean startReview = false;
    SettingsAPI set;
    String usrType = "new";
    private static final int LOGIN_REQUEST_CODE = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        set = new SettingsAPI(this);

        done = findViewById(R.id.rgstDone);

        name = findViewById(R.id.usrName);
        email = findViewById(R.id.usrEmail);
        mobile = findViewById(R.id.usrMobile);
        pass = findViewById(R.id.usrPwd);
        confpass = findViewById(R.id.usrPwdConfrm);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            serial = extra.getString("serial");
            type = extra.getString("type");
            startReview = extra.getBoolean("review");
        }

        if (isLoggedIn()) {
            startPostOp();
        } else {
            final TextView login = findViewById(R.id.register_to_login);
            final TextView lbl = findViewById(R.id.tv_login_switcher);

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    name.setVisibility(View.GONE);
                    mobile.setVisibility(View.GONE);
                    confpass.setVisibility(View.GONE);
                    login.setVisibility(View.GONE);
                    lbl.setVisibility(View.GONE);
                    pass.setFloatingLabelText("Password");
                    pass.setHint("Password");
                    usrType = "existing";
                }
            });

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (usrType.equals("new")) {
                        if (!name.getText().toString().contains(" "))
                            Snackbar.make(view, "Please enter full name", Snackbar.LENGTH_LONG).show();
                        else if (!email.getText().toString().contains("@") && !email.getText().toString().contains("."))
                            Snackbar.make(view, "Please enter valid email", Snackbar.LENGTH_LONG).show();
                        else if (!email.getText().toString().contains("."))
                            Snackbar.make(view, "Please enter valid email", Snackbar.LENGTH_LONG).show();
                        else if (mobile.getText().toString().startsWith("0"))
                            Snackbar.make(view, "Please enter valid mobile", Snackbar.LENGTH_LONG).show();
                        else if (mobile.getText().length() != 10)
                            Snackbar.make(view, "Please enter valid mobile", Snackbar.LENGTH_LONG).show();
                        else if (pass.getText().length() < 8)
                            Snackbar.make(view, "Password must be at least 8 characters", Snackbar.LENGTH_LONG).show();
                        else if (!pass.getText().toString().equals(confpass.getText().toString()))
                            Snackbar.make(view, "Passwords do not match", Snackbar.LENGTH_LONG).show();
                        else {
                            try {
                                rgstUri = getString(R.string.unidom) + "api/medility/register.php?name=" + URLEncoder.encode(name.getText().toString().trim(), "UTF-8") + "&email=" + URLEncoder.encode(email.getText().toString().trim(), "UTF-8") + "&mobile=" + URLEncoder.encode(mobile.getText().toString().trim(), "UTF-8") + "&password=" + URLEncoder.encode(pass.getText().toString().trim(), "UTF-8");
                            } catch (UnsupportedEncodingException ignored) {
                                rgstUri = getString(R.string.unidom) + "api/medility/register.php?name=" + name.getText().toString().trim() + "&email=" + email.getText().toString().trim() + "&mobile=" + mobile.getText().toString().trim() + "&password=" + pass.getText().toString().trim();
                            }
                            new Register().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    } else {
                        if (email.getText().toString().length() < 1 || pass.getText().length() < 8) {
                            Snackbar.make(view, getString(R.string.error_credential), Snackbar.LENGTH_LONG).show();
                        } else {
                            try {
                                lgUri = getString(R.string.unidom) + "api/medility/check_registration.php?email=" + URLEncoder.encode(email.getText().toString().trim(), "UTF-8") + "&password=" + URLEncoder.encode(pass.getText().toString().trim(), "UTF-8");
                            } catch (UnsupportedEncodingException ignored) {
                                lgUri = getString(R.string.unidom) + "api/medility/check_registration.php?email=" + email.getText().toString().trim() + "&password=" + pass.getText().toString().trim();
                            }
                            new Login().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                }
            });
        }
    }

    private void startPostOp() {
        if (startReview) {
            Intent rvw = new Intent(this, ReviewActivity.class);
            rvw.putExtra("serial", serial);
            rvw.putExtra("type", type);
            rvw.putExtra("uid", uid);
            startActivity(rvw);
            finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra("name", set.readSetting("name"));
            setResult(LOGIN_REQUEST_CODE, intent);
            finish();
        }
    }

    private boolean isLoggedIn() {
        uid = set.readSetting("uid");
        return !uid.equals("na");

    }

    private class Register extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            done.setText(R.string.uniLod);
            done.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(rgstUri);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder result = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    result.append(line);
                }
                uid = result.toString();
                stream.close();
            } catch (Exception e) {
                //Log.e("important", Log.getStackTraceString(e));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            switch (uid) {
                case "Fail":
                    done.setText(R.string.try_again);
                    done.setEnabled(true);
                    break;
                case "Registered":
                    done.setText(R.string.already_registered);
                    //done.setEnabled(true);
                    break;
                default:
                    set.addUpdateSettings("name", name.getText().toString());
                    set.addUpdateSettings("uid", uid);
                    startPostOp();
                    break;
            }
            super.onPostExecute(aVoid);
        }
    }

    private class Login extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            done.setText(R.string.uniLod);
            done.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(lgUri);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder result = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    result.append(line);
                }
                uid = result.toString();
                stream.close();
            } catch (Exception e) {
                //Log.e("important", Log.getStackTraceString(e));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!uid.equals("0")) {
                set.addUpdateSettings("name", uid.split("~")[1]);
                set.addUpdateSettings("uid", uid.split("~")[0]);
                startPostOp();
            } else {
                done.setText(R.string.wrong_credentials);
                done.setEnabled(true);
            }
            super.onPostExecute(aVoid);
        }
    }
}
