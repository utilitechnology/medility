package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.adapter.ReviewAdapter;
import com.utilitechnology.medility.fetcher.FetchReview;

import java.util.ArrayList;
import java.util.List;

public class AllReviewActivity extends AppCompatActivity {

    RecyclerView reviewList;
    ProgressBar reviewProg;
    private ReviewAdapter rAdapter;

    List<String> names = new ArrayList<>();
    List<String> ratings = new ArrayList<>();
    List<String> titles = new ArrayList<>();
    List<String> texts = new ArrayList<>();
    String serial;
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_review);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            serial = extra.getString("serial");
            type = extra.getInt("type");
        }

        reviewList = findViewById(R.id.reviewRecycler);
        reviewProg = findViewById(R.id.reviewProg);

        rAdapter = new ReviewAdapter(this, names, ratings, titles, texts);
        reviewList.setAdapter(rAdapter);
        reviewList.setKeepScreenOn(true);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        reviewList.setLayoutManager(mLayoutManager);
        reviewList.setItemAnimator(new DefaultItemAnimator());

        new FetchReview(this, reviewProg, rAdapter, names, ratings, titles, texts, serial, type).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
