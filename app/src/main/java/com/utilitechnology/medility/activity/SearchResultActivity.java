package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.fetcher.FetchSearchResult;
import com.utilitechnology.medility.model.MedicalFacilityModel;

import java.util.ArrayList;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {

    RecyclerView srchList;
    ProgressBar srchProgress;
    RecyclerAdapter srchAdapter;
    ImageView sadSmiley;

    int itemType;
    String searchParam;

    private List<MedicalFacilityModel> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        srchList = findViewById(R.id.searchResultRecycler);
        srchProgress = findViewById(R.id.srProg);
        sadSmiley = findViewById(R.id.empty_smiley);

        if (getIntent() != null) {
            itemType = getIntent().getExtras().getInt("type");
            searchParam = getIntent().getStringExtra("param");
        }
    }

    @Override
    protected void onResume() {
        srchAdapter = new RecyclerAdapter(this, items);
        srchList.setAdapter(srchAdapter);
        srchList.setKeepScreenOn(true);

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        srchList.setLayoutManager(mLayoutManager);
        srchList.setItemAnimator(new DefaultItemAnimator());

        new FetchSearchResult(this, srchProgress, srchAdapter, sadSmiley, items, searchParam).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        super.onResume();
    }
}
