package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.fetcher.FetchOnlinePharma;

public class OnlinePharmaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_pharma);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new FetchOnlinePharma(this, findViewById(android.R.id.content)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
