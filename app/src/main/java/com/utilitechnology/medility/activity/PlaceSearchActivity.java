package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.adapter.PlaceSearchAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

public class PlaceSearchActivity extends AppCompatActivity {

    MaterialEditText placeSrch;
    ListView srchSug;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        placeSrch = findViewById(R.id.placeSearchBox);
        srchSug = findViewById(R.id.placeSuggestion);
        progress = findViewById(R.id.placeProg);

        placeSrch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    new FetchPlaces(charSequence).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                catch (RejectedExecutionException ignored){}
//                Snackbar.make(findViewById(android.R.id.content),charSequence,Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    class FetchPlaces extends AsyncTask<Void,Void,Void>
    {
        List<String> completePlace=new ArrayList<>();
        String param;

        FetchPlaces(CharSequence query)
        {
            param=query.toString();
        }
        @Override
        protected void onPreExecute() {
            progress.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(PlaceSearchActivity.this.getString(R.string.unidom) + "api/medility/search_place.php?param="+param);
//                URL url = new URL("http://www.utilitechnology.com/api/medility/search_place.php?param="+param);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                for (String line; (line = reader.readLine()) != null; ) {
                    completePlace.add(line);
                }
                stream.close();
            }
            catch (IOException ioex) {
                completePlace.add("No match~No result matching your search parameter, try searching with pincode~0,0");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progress.setVisibility(View.GONE);
            srchSug.setAdapter(new PlaceSearchAdapter(PlaceSearchActivity.this, completePlace.toArray(new String[completePlace.size()])));
            super.onPostExecute(aVoid);
        }
    }
}
