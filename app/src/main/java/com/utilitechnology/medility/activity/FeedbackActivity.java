package com.utilitechnology.medility.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.utilitechnology.medility.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class FeedbackActivity extends AppCompatActivity {

    TextView feedText;
    Button feedBtn;
    String sbFdUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        feedText = findViewById(R.id.feed_text);
        feedBtn = findViewById(R.id.sub_feed);
        feedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sbFdUri = getString(R.string.unidom) + "api/medility/write_feedback.php?text=" + URLEncoder.encode(feedText.getText().toString().trim(), "UTF-8");
                    new submitFeedback().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } catch (UnsupportedEncodingException ignored) {}
            }
        });

    }

    private class submitFeedback extends AsyncTask<Void, Void, Void> {
        String submitSuccess;
        @Override
        protected void onPreExecute() {
            feedBtn.setText(R.string.uniLod);
            feedBtn.setEnabled(false);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(sbFdUri);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                InputStream stream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder result = new StringBuilder();
                for (String line; (line = reader.readLine()) != null; ) {
                    result.append(line);
                }
                submitSuccess = result.toString();
                stream.close();
            } catch (Exception e) {
                //Log.e("important", Log.getStackTraceString(e));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (submitSuccess.equals("Success"))
                feedBtn.setText("Got it, thanks");
            else {
                feedBtn.setText(R.string.try_again);
                feedBtn.setEnabled(true);
            }
            super.onPostExecute(aVoid);
        }
    }
}
