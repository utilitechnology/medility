package com.utilitechnology.medility.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.model.MedicalFacilityModel;
import com.utilitechnology.medility.util.Constants;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.util.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Created by Bibaswann on 23-05-2017.
 */

public class FetchSearchResult extends AsyncTask<Void, Void, Void> {

    private Context mContext;
    private ProgressBar waiting;
    private ImageView sadSmiley;

    private Constants.UserTypes itemType;
    private String searchParam;

    private List<MedicalFacilityModel> items;
    private RecyclerAdapter rAdapter;

    public FetchSearchResult(Context context, ProgressBar prog, RecyclerAdapter adapter, ImageView smiley, List facilities, String srchPrm) {
        mContext = context;
        waiting = prog;
        sadSmiley = smiley;
        searchParam = srchPrm;
        rAdapter = adapter;
        items = facilities;
    }

    @Override
    protected void onPreExecute() {
        waiting.setVisibility(View.VISIBLE);
        items.clear();
        items.clear();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        switch (itemType) {
            case TYPE_HOSPITAL:
                try {
                    getSearchItems(mContext.getString(R.string.unidom) + "api/medility/search_hospitals.php?param=" + searchParam + "&limit=100");
                } catch (OutOfMemoryError | ConcurrentModificationException oom) {
                }
                break;
            case TYPE_PHARMACY:
                try {
                    getSearchItems(mContext.getString(R.string.unidom) + "api/medility/search_pharma.php?param=" + searchParam + "&limit=100");
                } catch (OutOfMemoryError | ConcurrentModificationException oom) {
                }
                break;
            case TYPE_LAB:
                try {
                    getSearchItems(mContext.getString(R.string.unidom) + "api/medility/search_clinic.php?param=" + searchParam + "&limit=100");
                } catch (OutOfMemoryError | ConcurrentModificationException oom) {
                }
                break;
            case TYPE_BLOOD_BANK:
                try {
                    getSearchItems(mContext.getString(R.string.unidom) + "api/medility/search_blood_bank.php?param=" + searchParam + "&limit=100");
                } catch (OutOfMemoryError | ConcurrentModificationException oom) {
                }
                break;
            default:
                break;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        waiting.setVisibility(View.GONE);
        rAdapter.notifyDataSetChanged();
        if (items.size() == 0)
            sadSmiley.setVisibility(View.VISIBLE);
        super.onPostExecute(aVoid);
    }

    private void getSearchItems(String urlStr) {
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            InputStream stream = conn.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            for (String line; (line = reader.readLine()) != null; ) {
//                    String[] lineParts=line.split(",");
//                    mFac.add(lineParts[1]+"["+lineParts[2]+"]");
                line = line.replace("&amp;", "&");
                String[] lineParts = line.split("~");
                if (!lineParts[5].contains("."))
                    lineParts[5] = lineParts[5] + ".00";
                items.add(new MedicalFacilityModel(lineParts[1], lineParts[6], lineParts[5], Long.parseLong(lineParts[0]), lineParts[4], Util.INSTANCE.convertStringToEnum(lineParts[3])));
            }
            stream.close();
        } catch (NullPointerException | IOException e) {
            //Log.e("important", "getFacilities: ");
        }
    }
}
