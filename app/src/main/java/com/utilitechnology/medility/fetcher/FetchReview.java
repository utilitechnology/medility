package com.utilitechnology.medility.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;

import com.utilitechnology.medility.R;
import com.utilitechnology.medility.adapter.ReviewAdapter;
import com.utilitechnology.medility.parser.ParseReviewXML;

import java.util.List;

/**
 * Created by Bibaswann on 10-12-2016.
 */

public class FetchReview extends AsyncTask<Void, Void, Void> {

    private Context mContext;
    private ProgressBar waiting;

    private ReviewAdapter rAdapter;
    private List<String> name, rating, title, text;

    private String ser;
    int tpe;

    public FetchReview(Context context, ProgressBar prog, ReviewAdapter adapter, List names, List ratings, List titles, List texts, String serial, int type) {
        mContext = context;
        waiting = prog;
        rAdapter = adapter;

        name = names;
        rating = ratings;
        title = titles;
        text = texts;

        ser = serial;
        tpe = type;
    }

    @Override
    protected void onPreExecute() {
        waiting.setVisibility(View.VISIBLE);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String fetchUrl;
        if (tpe == 0)
            fetchUrl = mContext.getString(R.string.unidom) + "api/medility/read_my_review.php?serial=" + ser;
        else
            fetchUrl = mContext.getString(R.string.unidom) + "api/medility/list_review.php?serial=" + ser + "&type=" + tpe;
        ParseReviewXML prx = new ParseReviewXML(fetchUrl);
        prx.fetchXML();
        while (prx.parsingInComplete) ;
        if (prx.getReviewer().size() > 0)
            name.addAll(prx.getReviewer());
        if (prx.getReviewed().size() > 0)
            name.addAll(prx.getReviewed());
        rating.addAll(prx.getRating());
        title.addAll(prx.getTitle());
        text.addAll(prx.getBody());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        waiting.setVisibility(View.GONE);
        rAdapter.notifyDataSetChanged();
        super.onPostExecute(aVoid);
    }
}
