package com.utilitechnology.medility.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.utilitechnology.medility.model.MedicalFacilityModel;
import com.utilitechnology.medility.setting.BookmarkAPI;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.util.Constants;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.parser.ParseBloodBankXML;
import com.utilitechnology.medility.parser.ParseHospitalXML;
import com.utilitechnology.medility.parser.ParseLabXML;
import com.utilitechnology.medility.parser.ParsePharmaXML;
import com.utilitechnology.medility.util.Util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Bibaswann on 14-03-2017.
 */

public class FetchBookmark extends AsyncTask<Void, Void, Void> {

    private Context mContext;
    private ProgressBar waiting;
    private ImageView sadSmiley;

    private ArrayList<MedicalFacilityModel> facilityModels;

    private RecyclerAdapter rAdapter;

    BookmarkAPI bkm;

    public FetchBookmark(Context context, ProgressBar prog, RecyclerAdapter adapter, ImageView smiley, ArrayList<MedicalFacilityModel> items) {
        mContext = context;
        waiting = prog;
        sadSmiley = smiley;
        rAdapter = adapter;
        facilityModels = items;
        bkm = new BookmarkAPI(mContext);
    }

    @Override
    protected void onPreExecute() {
        waiting.setVisibility(View.VISIBLE);
        facilityModels.clear();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String serial;
        Constants.UserTypes type;
        Map bookMarks = bkm.readAllSettings();
        Iterator it = bookMarks.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            type = Util.INSTANCE.convertStringToEnum(pair.getValue().toString().split(",")[0]);
            serial = pair.getValue().toString().split(",")[1];
            String fetchUrl;
            switch (type) {
                case TYPE_HOSPITAL:
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/details_hospital.php?serial=" + serial;
                    ParseHospitalXML phx = new ParseHospitalXML(fetchUrl);
                    phx.fetchXML();
                    while (phx.parsingInComplete) ;
                    facilityModels.add(phx.getHospitalModel());
                    break;
                case TYPE_PHARMACY:
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/details_pharma.php?serial=" + serial;
                    ParsePharmaXML ppx = new ParsePharmaXML(fetchUrl);
                    ppx.fetchXML();
                    while (ppx.parsingInComplete) ;
                    facilityModels.add(ppx.getPharmaModel());
                    break;
                case TYPE_LAB:
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/details_clinic.php?serial=" + serial;
                    ParseLabXML plx = new ParseLabXML(fetchUrl);
                    plx.fetchXML();
                    while (plx.parsingInComplete) ;
                    facilityModels.add(plx.getLabModel());
                    break;
                case TYPE_BLOOD_BANK:
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/details_blood_bank.php?serial=" + serial;
                    ParseBloodBankXML pbx = new ParseBloodBankXML(fetchUrl);
                    pbx.fetchXML();
                    while (pbx.parsingInComplete) ;
                    facilityModels.add(pbx.getBloodBankModel());
                    break;
                default:
                    break;
            }
        }
//        Log.e("important", "doInBackground: "+iName.size());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        waiting.setVisibility(View.GONE);
        rAdapter.notifyDataSetChanged();
        if (facilityModels.size() == 0)
            sadSmiley.setVisibility(View.VISIBLE);
        super.onPostExecute(aVoid);
    }
}
