package com.utilitechnology.medility.fetcher;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.utilitechnology.medility.model.MedicalFacilityModel;
import com.utilitechnology.medility.parser.ParseListXML;
import com.utilitechnology.medility.setting.LatLonCachingAPI;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.util.Constants;
import com.utilitechnology.medility.adapter.RecyclerAdapter;
import com.utilitechnology.medility.parser.ParseBloodBankXML;
import com.utilitechnology.medility.parser.ParseHospitalXML;
import com.utilitechnology.medility.parser.ParseLabXML;
import com.utilitechnology.medility.parser.ParsePharmaXML;
import com.utilitechnology.medility.widget.ProgressDialog;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Created by Bibaswann on 28-10-2016.
 */

public class FetchItems extends AsyncTask<Void, Void, Void> {

    private Constants.UserTypes itemType;
    private Context mContext;
    private String pageNum;
    private ProgressDialog waiting;

    LatLonCachingAPI llc;
    String pincode;
    ArrayList<MedicalFacilityModel> facilityModels;

    private RecyclerAdapter rAdapter;

    public FetchItems(Context context, ProgressDialog prog, RecyclerAdapter adapter, Constants.UserTypes type, String page, ArrayList facilities) {
        mContext = context;
        waiting = prog;
        pageNum = page;
        itemType = type;

        rAdapter = adapter;
        facilityModels = facilities;

        llc = new LatLonCachingAPI(mContext);
        pincode = llc.readPincode();
    }

    @Override
    protected void onPreExecute() {
        waiting.showDialog(mContext);
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String fetchUrl;
        // TODO: 27/11/18 use retrofit to convert automatically
        switch (itemType) {
            case TYPE_HOSPITAL:
                try {
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/list_hospitals.php?page=" + pageNum + "&pincode=" + pincode;
//                    ParseHospitalXML phx = new ParseHospitalXML(fetchUrl);
//                    phx.fetchXML();
//                    while (phx.parsingInComplete) ;
//                    iName.addAll(phx.getIname());
//                    iAddr.addAll(phx.getAddress());
//                    iRat.addAll(phx.getRating());
//                    iSerial.addAll(phx.getSerial());
//                    iImg.addAll(phx.getDp());
//                    for (String ser : iSerial) {
//                        iType.add(itemType);
//                    }
                    getList(fetchUrl, Constants.HOSPITAL_XML_STRING);


                } catch (OutOfMemoryError | ConcurrentModificationException | ArrayIndexOutOfBoundsException oom) {
                }
                break;
            case TYPE_PHARMACY:
                try {
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/list_pharma.php?page=" + pageNum + "&pincode=" + pincode;
//                    ParsePharmaXML pfx = new ParsePharmaXML(fetchUrl);
//                    pfx.fetchXML();
//                    while (pfx.parsingInComplete) ;
//                    iName.addAll(pfx.getIname());
//                    iAddr.addAll(pfx.getAddress());
//                    iRat.addAll(pfx.getRating());
//                    iSerial.addAll(pfx.getSerial());
//                    iImg.addAll(pfx.getDp());
//                    for (String ser : iSerial) {
//                        iType.add(itemType);
//                    }
                    getList(fetchUrl, Constants.PHARMACY_XML_STRING);
                    //Log.e("important","size is "+iName.size());
                } catch (OutOfMemoryError | ConcurrentModificationException | ArrayIndexOutOfBoundsException oom) {
                }
                break;
            case TYPE_LAB:
                try {
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/list_clinics.php?page=" + pageNum + "&pincode=" + pincode;
//                    ParseLabXML plx = new ParseLabXML(fetchUrl);
//                    plx.fetchXML();
//                    while (plx.parsingInComplete) ;
//                    iName.addAll(plx.getIname());
//                    iAddr.addAll(plx.getAddress());
//                    iRat.addAll(plx.getRating());
//                    iSerial.addAll(plx.getSerial());
//                    iImg.addAll(plx.getDp());
//                    for (String ser : iSerial) {
//                        iType.add(itemType);
//                    }
                    getList(fetchUrl, Constants.LAB_XML_STRING);
                    //Log.e("important","size is "+iName.size());
                } catch (OutOfMemoryError | ConcurrentModificationException | ArrayIndexOutOfBoundsException oom) {
                }
                break;
            case TYPE_BLOOD_BANK:
                try {
                    fetchUrl = mContext.getString(R.string.unidom) + "api/medility/list_blood_bank.php?page=" + pageNum + "&pincode=" + pincode;
//                    ParseBloodBankXML pbx = new ParseBloodBankXML(fetchUrl);
//                    pbx.fetchXML();
//                    while (pbx.parsingInComplete) ;
//                    iName.addAll(pbx.getIname());
//                    iAddr.addAll(pbx.getAddress());
//                    iRat.addAll(pbx.getRating());
//                    iSerial.addAll(pbx.getSerial());
//                    iImg.addAll(pbx.getDp());
//                    for (String ser : iSerial) {
//                        iType.add(itemType);
//                    }
                    getList(fetchUrl, Constants.BLOODBANK_XML_STRING);
                    //Log.e("important","size is "+iName.size());
                } catch (OutOfMemoryError | ConcurrentModificationException oom) {
                }
                break;
            default:
                break;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        waiting.hideDialog();
        rAdapter.notifyDataSetChanged();
        //todo use in khobor
        //rAdapter.notifyItemChanged(0,rAdapter.getItemCount());
        super.onPostExecute(aVoid);
    }

    private void getList(String fetchUrl, String type) {
        ParseListXML parseListXML = new ParseListXML(fetchUrl, type);
        parseListXML.fetchXML();
        while (parseListXML.getParsingInComplete()) ;
        facilityModels.addAll(parseListXML.getFacilityList());
    }
}
