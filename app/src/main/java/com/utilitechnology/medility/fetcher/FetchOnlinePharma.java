package com.utilitechnology.medility.fetcher;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.utilitechnology.medility.R;
import com.utilitechnology.medility.parser.ParseOnlinePharma;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by B Bandyopadhyay on 24-08-2017.
 */

public class FetchOnlinePharma extends AsyncTask<Void, Void, Void> {
    Context mContext;
    View mView;
    LinearLayout phod;
    ProgressBar progress;
    ParseOnlinePharma pop;
    private List<String> names, dps, serials, urls, phones;
    FetchOnlinePharma asyncCountdown;

    public FetchOnlinePharma(Context context, View v) {
        mContext = context;
        mView = v;
        names = new ArrayList<>();
        dps = new ArrayList<>();
        serials = new ArrayList<>();
        urls = new ArrayList<>();

        phod = (LinearLayout) mView.findViewById(R.id.pharma_holder);
        progress = (ProgressBar) mView.findViewById(R.id.opProg);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        asyncCountdown = this;
        new CountDownTimer(10000, 10000) {
            public void onTick(long millisUntilFinished) {
                // You can monitor the progress here as well by changing the onTick() time
            }

            //Todo: loop continues after timeout, and app crashees if no internet
            public void onFinish() {
                // stop async task if not in progress
                if (asyncCountdown.getStatus() == AsyncTask.Status.RUNNING) {
                    asyncCountdown.cancel(true);

                    Snackbar.make(mView, "Could not connect, please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }
        }.start();
    }

    @Override
    protected Void doInBackground(Void... params) {
        pop = new ParseOnlinePharma(mContext.getString(R.string.pharma_list_url));
        pop.fetchXML();
        while (pop.parsingInComplete) ;
        serials = pop.getpSer();
        names = pop.getpName();
        dps = pop.getpDp();
        urls = pop.getpUrl();
        phones = pop.getpPhone();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progress.setVisibility(View.GONE);
        phod.removeAllViews();

        for (final String serial : serials) {
            final int index = serials.indexOf(serial);
            LinearLayout ll = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.online_pharma_item, phod, false);
            ImageView img = (ImageView) ll.findViewById(R.id.pdp);
            Picasso.with(mContext).load(mContext.getString(R.string.unidom)+dps.get(index)).resize(200,80).into(img);

            Button online = (Button) ll.findViewById(R.id.order_online);
            Button phone = (Button) ll.findViewById(R.id.order_phone);

            if (phones.get(index).equals("NA")) {
                phone.setEnabled(false);
                phone.setTextColor(ContextCompat.getColor(mContext, R.color.pure_grey));
            }

            phone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+phones.get(index))));
                }
            });

            online.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urls.get(index))));
                }
            });
            phod.addView(ll);

        }

        super.onPostExecute(aVoid);
    }
}
