package com.utilitechnology.medility.model

import com.utilitechnology.medility.util.Constants

/**
Created by bibaswann on 30/11/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class LabModel : MedicalFacilityModel {
    var pincode: String? = null
    var coordinate: String? = null
    var dp: String? = null
    var category: String? = null
    var phone: String? = null
    var mobile: String? = null
    var openHours: String? = null
    var timesRated: String? = null
    var timesReviewed: String? = null

    constructor() {
        this.type = Constants.UserTypes.TYPE_LAB
    }

    constructor(name: String, addrress: String, rating: String, serial: Long, img: String, type: Constants.UserTypes, pincode: String?, coordinate: String?, dp: String?, category: String?, phone: String?, mobile: String?, openHours: String?, rated: String?, reviewed: String?) : super(name, addrress, rating, serial, img, type) {
        this.pincode = pincode
        this.coordinate = coordinate
        this.dp = dp
        this.category = category
        this.phone = phone
        this.mobile = mobile
        this.openHours = openHours
        this.timesRated = rated
        this.timesReviewed = reviewed
    }


}