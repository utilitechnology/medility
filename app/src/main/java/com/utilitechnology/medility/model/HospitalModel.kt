package com.utilitechnology.medility.model

import com.utilitechnology.medility.util.Constants

/**
Created by bibaswann on 29/11/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class HospitalModel : MedicalFacilityModel {
    var pincode: String? = null
    var coordinate: String? = null
    var dp: String? = null
    var discipline: String? = null
    var phone: String? = null
    var mobile: String? = null
    var timesRated: String? = null
    var timesReviewed: String? = null
    var category: String? = null
    var establishment: String? = null
    var emergency: String? = null
    var ambulance: String? = null
    var bloodbank: String? = null
    var helpline: String? = null
    var tollfree: String? = null
    var fax: String? = null
    var email: String? = null
    var website: String? = null
    var specialities: String? = null
    var facilities: String? = null
    var reg: String? = null
    var estd: String? = null
    var doc: String? = null
    var bed: String? = null
    var pvtWard: String? = null
    var emergencyServices: String? = null
    var tariff: String? = null

    constructor() {
        this.type = Constants.UserTypes.TYPE_HOSPITAL
    }

    constructor(name: String, addrress: String, rating: String, serial: Long, img: String, type: Constants.UserTypes, pincode: String?, coordinate: String?, dp: String?, discipline: String?, phone: String?, mobile: String?, rated: String?, reviewed: String?, category: String?, establishment: String?, emergency: String?, ambulance: String?, bloodbank: String?, helpline: String?, tollfree: String?, fax: String?, email: String?, website: String?, specialities: String?, facilities: String?, reg: String?, estd: String?, doc: String?, bed: String?, pvtWard: String?, emergencyServices: String?, tariff: String?) : super(name, addrress, rating, serial, img, type) {
        this.pincode = pincode
        this.coordinate = coordinate
        this.dp = dp
        this.discipline = discipline
        this.phone = phone
        this.mobile = mobile
        this.timesRated = rated
        this.timesReviewed = reviewed
        this.category = category
        this.establishment = establishment
        this.emergency = emergency
        this.ambulance = ambulance
        this.bloodbank = bloodbank
        this.helpline = helpline
        this.tollfree = tollfree
        this.fax = fax
        this.email = email
        this.website = website
        this.specialities = specialities
        this.facilities = facilities
        this.reg = reg
        this.estd = estd
        this.doc = doc
        this.bed = bed
        this.pvtWard = pvtWard
        this.emergencyServices = emergencyServices
        this.tariff = tariff
    }

}