package com.utilitechnology.medility.model

import com.utilitechnology.medility.util.Constants

/**
Created by bibaswann on 27/11/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


open class MedicalFacilityModel : BaseModel {
    var name: String? = null
    var address: String? = null
    var rating: String? = null
    var serial: Long? = null
    var img: String? = null
    var type: Constants.UserTypes? = null

    constructor()

    constructor(name: String, address: String, rating: String, serial: Long, img: String, type: Constants.UserTypes) : super() {
        this.name = name
        this.address = address
        this.rating = rating
        this.serial = serial
        this.img = img
        this.type = type
    }
}