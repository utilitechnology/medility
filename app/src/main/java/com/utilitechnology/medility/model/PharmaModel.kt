package com.utilitechnology.medility.model

import com.utilitechnology.medility.util.Constants

/**
Created by bibaswann on 27/11/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class PharmaModel : MedicalFacilityModel {
    var pincode: String? = null
    var coordinate: String? = null
    var dp: String? = null
    var discipline: String? = null
    var phone: String? = null
    var mobile: String? = null
    var is24: String? = null
    var timesRated: String? = null
    var timesReviewed: String? = null

    constructor() {
        this.type = Constants.UserTypes.TYPE_PHARMACY
    }

    constructor(name: String, addrress: String, rating: String, serial: Long, img: String, type: Constants.UserTypes, address: String?, pincode: String?, coordinate: String?, dp: String?, discipline: String?, phone: String?, mobile: String?, is24: String?, rated: String?, timesReviewed: String?) : super(name, addrress, rating, serial, img, type) {
        this.address = address
        this.pincode = pincode
        this.coordinate = coordinate
        this.dp = dp
        this.discipline = discipline
        this.phone = phone
        this.mobile = mobile
        this.is24 = is24
        this.timesRated = rated
        this.timesReviewed = timesReviewed
    }


}