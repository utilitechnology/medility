package com.utilitechnology.medility.model

import com.utilitechnology.medility.util.Constants

/**
Created by bibaswann on 30/11/18.
DO NOT MODIFY WITHOUT PROPER DISCUSSION
 */


class BloodBankModel : MedicalFacilityModel {
    var pincode: String? = null
    var coordinate: String? = null
    var dp: String? = null
    var phone: String? = null
    var mobile: String? = null
    var timesRated: String? = null
    var timesReviewed: String? = null
    var category: String? = null
    var helpline: String? = null
    var fax: String? = null
    var email: String? = null
    var website: String? = null
    var openHours: String? = null
    var noName: String? = null
    var noEmail: String? = null
    var noPhone: String? = null
    var noMobile: String? = null
    var bloodComponent: String? = null
    var apheresis: String? = null
    var license: String? = null

    constructor() {
        this.type = Constants.UserTypes.TYPE_BLOOD_BANK
    }

    constructor(name: String, addrress: String, rating: String, serial: Long, img: String, type: Constants.UserTypes, pincode: String?, coordinate: String?, dp: String?, phone: String?, mobile: String?, rated: String?, reviewed: String?, category: String?, helpline: String?, fax: String?, email: String?, website: String?, openHours: String?, noName: String?, noEmail: String?, noPhone: String?, noMobile: String?, blood_component: String?, ap: String?, license: String?) : super(name, addrress, rating, serial, img, type) {
        this.pincode = pincode
        this.coordinate = coordinate
        this.dp = dp
        this.phone = phone
        this.mobile = mobile
        this.timesRated = rated
        this.timesReviewed = reviewed
        this.category = category
        this.helpline = helpline
        this.fax = fax
        this.email = email
        this.website = website
        this.openHours = openHours
        this.noName = noName
        this.noEmail = noEmail
        this.noPhone = noPhone
        this.noMobile = noMobile
        this.bloodComponent = blood_component
        this.apheresis = ap
        this.license = license
    }

}